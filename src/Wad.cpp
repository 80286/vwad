#include "Wad.h"
#include <map>

static int wad_verbose = 0;

static void wadDebug(const char *f, ...) {
	va_list vl;

    if(wad_verbose == 0) {
        return;
    }

	va_start(vl, f);
	vprintf(f, vl);
	va_end(vl); 
}

static Image *readFlat(FILE *f, const filelump_t *lump) {
    wadDebug("readFlat('%s'); size: %d;\n", str8((char *)lump->name), lump->size);

    fseek(f, lump->filepos, 0);

    Image* img = (Image *)Malloc(sizeof(Image));
    img->m_data = (unsigned char *)Malloc(sizeof(unsigned char) * 4096);
    img->m_width  = 64;
    img->m_height = 64;

    fread(img->m_data, sizeof(unsigned char) * 4096, 1, f);

    return img;
}

/* http://doom.wikia.com/wiki/Picture_format */
static Image *readImage(FILE *f, const filelump_t *lump) {
    fseek(f, lump->filepos, 0);
    /* wadDebug("readImage('%s'); size: %d;\n", lump->name, lump->size); */

    unsigned char* data = (unsigned char *)Malloc(sizeof(unsigned char) * (size_t)lump->size);

    fread(data, sizeof(unsigned char) * lump->size, 1, f);

    unsigned char* pos = data;

    Image* img = (Image *)Malloc(sizeof(Image));
    memcpy(&(img->m_width), pos + 0, 2);
    memcpy(&(img->m_height), pos + 2, 2);
    pos += 8; // width(2) + height(2) + left(2) + top(2) = 8

    const auto imgSize = sizeof(unsigned char) * (size_t)(img->m_width * img->m_height);

    img->m_data = (unsigned char *)Malloc(imgSize);
    memset(img->m_data, 247, imgSize);

    auto column_array = (unsigned int *)Malloc((size_t)(img->m_width * sizeof(unsigned int)));

    memcpy(column_array, pos, img->m_width * sizeof(unsigned int));
    pos += img->m_width * sizeof(unsigned int);

    for(int i = 0; i < img->m_width; ++i) {
        if(column_array[i] < (unsigned int)lump->size) {
            pos = data + column_array[i];
        } else {
            continue;
        }

        unsigned char rowstart = 0;

        while(rowstart != 255) {
            rowstart = *pos;
            pos++;
            if(rowstart == 255) {
                break;
            }

            unsigned char pixel_count = *pos;
            pos += 2; // skip pixel_count(1) + dummy value(1)

            for(int j = 0; j < pixel_count; ++j) {
                unsigned char pixel = *pos;
                pos++;
                img->m_data[(j + rowstart) * img->m_width + i] = pixel;
            }
            pos++; // skip dummy value(1)
        }
    }

    Free(column_array);
    Free(data);

    return img;
}

Image::Image() {
    m_data = nullptr;
    m_width = 0;
    m_height = 0;
    m_name[0] = '\0';
}

Image::Image(const int width, const int height, uint8_t* data, const char* name) {
    m_width = width;
    m_height = height;
    m_data = data;
    strncpy(m_name, name, 8);
}

void Image::freeImage(Image* img) {
    Free(img->m_data);
    img->m_data = nullptr;
}

bool line_t::isOneSided() {
    return lside == nullptr;
}

void Wad::paletteIndexToRGB(const unsigned char index, unsigned char *color) {
    const short i = 3 * index;

    color[2] = m_palette[i + 0];
    color[1] = m_palette[i + 1];
    color[0] = m_palette[i + 2];
}

/**************************************************
 * getColormapIndex();
 * 
 * @param map
 *      map \in [0, 31] (0 - bright, 31 - dark).
 * @param paletteIndex
 *      palleteIndex \in [0, 255]
 *************************************************/
unsigned char Wad::getColormapIndex(const unsigned int map, const unsigned char paletteIndex) {
    return m_colormap[256 * map + paletteIndex];
}

bool Wad::isMapName(const char *str) {
    return
        (strlen(str) == 5 &&
         strncmp(str, "MAP", 3) == 0 &&
         isdigit(str[3]) &&
         isdigit(str[4]))
        || 
        (strlen(str) == 4 &&
         str[0] == 'E' &&
         str[2] == 'M' &&
         isdigit(str[1]) &&
         isdigit(str[3]));
}


sector_t *getSectorByTag(sector_t *sectors, int sectors_len, short tag) {
    for(int i = 0; i < sectors_len; i++) {
        if(sectors[i].tag == tag) {
            return sectors + i;
        }
    }

    printf("getSectorByTag(); Sector tagged by %d does'nt exist.\n", tag);
    return NULL;
}

thing_t *getThingByType(thing_t *things, int things_len, short type) {
    for(int i = 0; i < things_len; i++) {
        if(things[i].type == type) {
            return things + i;
        }
    }

    printf("getThingByType(); Unable to find thing type %d\n", type);
    return NULL;
}

void Wad::listContent() {
    if(!isOpen()) {
        return;
    }

    printf("--------------------------- \n");
    printf("Type: %s\n", m_info.id);
    printf("Numlumps: %d\n", m_info.numlumps);
    printf("Infotableofs: %d\n", m_info.infotableofs);

    for(auto &lumpRef: m_dir) {
        auto lump = &lumpRef;

        if(lump->size == 0) {
            printf("[%9s] { Size: 0; ... };\n", lump->name);
            continue;
        }

        printf("[%9s] {\n\tFilepos: %d;\n\tSize: %d;\n\tData: %p(%d element%s;\n}\n",
                str8(lump->name),
                lump->filepos,
                lump->size,
                lump->data,
                lump->data_len,
                lump->data_len == 1 ? ")" : "s)");
    }
}

void Wad::thingInfo(void* self, thing_t *thing) {
    if(wad_verbose < 2) {
        return;
    }

    wadDebug("(%d, %d, %d, %d, %d);\n", 
            thing->x, 
            thing->y,
            thing->angle,
            thing->type,
            thing->flags);
}

void Wad::linedefInfo(void* self, linedef_t *linedef) {
    if(wad_verbose < 2) {
        return;
    }

    wadDebug("(%d, %d, %d, %d, %d, %d, %d);\n", 
            linedef->start,
            linedef->end,
            linedef->flags,
            linedef->type,
            linedef->sectortag,
            linedef->rightsdef,
            linedef->leftsdef);
}

void Wad::sidedefInfo(void* self, sidedef_t *sidedef) {
    if(wad_verbose < 2) {
        return;
    }

    wadDebug("(%4d, %4d, %9s, %9s, %9s, %d);\n",
            sidedef->x,
            sidedef->y,
            str8(sidedef->uppertex),
            str8(sidedef->middletex),
            str8(sidedef->lowertex),
            sidedef->sector);
}

void Wad::vertexInfo(void* self, vertex_t *vertex) {
    if(wad_verbose < 2) {
        return;
    }

    wadDebug("(%d, %d);\n", vertex->x, vertex->y);
}

void Wad::segInfo(void* self, seg_t *seg) {
    if(wad_verbose < 2) {
        return;
    }

    wadDebug("(%d, %d, %d, %d, %d, %d);\n", 
            seg->start,
            seg->end,
            seg->angle,
            seg->linedef,
            seg->direction,
            seg->offset);
}

void Wad::sectorInfo(void* self, sector_t *sector) {
    if(wad_verbose < 2) {
        return;
    }

    wadDebug("(%4d, %4d, %9s, %9s, %4d, %4d, %4d);\n", 
            sector->floorheight,
            sector->ceilheight,
            str8(sector->floortex),
            str8(sector->ceiltex),
            sector->lightlevel,
            sector->type,
            sector->tag);
}

void Wad::subSectorInfo(void* self, subsector_t *subsector) {
    if(wad_verbose < 2) {
        return;
    }

    wadDebug("(%4d, %4d);\n", subsector->segcount, subsector->seg);
}

void Wad::nodeInfo(void* self, node_t *node) {
    static const char *bbf = "\t%cbox: top:%d, bottom:%d, left: %d, right: %d\n";
    if(wad_verbose < 2) {
        return;
    }

    wadDebug("(part_x:%d, part_y:%d, cx:%d, cy:%d, left:%d, right: %d\n",
            node->x_part,
            node->y_part,
            node->cx,
            node->cy,
            node->left,
            node->right);
    wadDebug(bbf, 'r',
            node->rbox.top,
            node->rbox.bottom,
            node->rbox.left,
            node->rbox.right);
    wadDebug(bbf, 'l',
            node->lbox.top,
            node->lbox.bottom,
            node->lbox.left,
            node->lbox.right);
}

template<class T>
void readLumps(void* self, FILE* f, filelump_t* lump, void (*printFunc)(void*, T*), const char* typeStr) {
    const int length = lump->size / sizeof(T);
    auto wad = (Wad*)self;

    lump->data_len = length;
    lump->data = Malloc(sizeof(T) * length);
    T* data = (T*)lump->data;

    fseek(f, lump->filepos, 0);
    wadDebug("readLumps(%s); #%d\n", typeStr, length);

    for(int i = 0; i < length; ++i) {
        fread(data + i, sizeof(T), 1, f);
        printFunc(self, data + i);
    }
}

void Wad::readThings(void* self, FILE *f, filelump_t *lump) {
    readLumps<thing_t>(self, f, lump, Wad::thingInfo, "thing_t");
}

void Wad::readLinedefs(void* self, FILE *f, filelump_t *lump) {
    readLumps<linedef_t>(self, f, lump, Wad::linedefInfo, "linedef_t");
}

void Wad::readSidedefs(void* self, FILE *f, filelump_t *lump) {
    readLumps<sidedef_t>(self, f, lump, Wad::sidedefInfo, "sidedef_t");
}

void Wad::readVertexes(void* self, FILE *f, filelump_t *lump) {
    readLumps<vertex_t>(self, f, lump, Wad::vertexInfo, "vertex_t");
}

void Wad::readSegments(void* self, FILE *f, filelump_t *lump) {
    readLumps<seg_t>(self, f, lump, Wad::segInfo, "seg_t");
}

void Wad::readSectors(void* self, FILE *f, filelump_t *lump) {
    readLumps<sector_t>(self, f, lump, Wad::sectorInfo, "sector_t");
}

void Wad::readPNames(void* self, FILE *f, filelump_t *lump) {
    int nummappatches, i;
    char **array;
    size_t sl;

    sl = (size_t)(sizeof(char) * 8);
    fseek(f, lump->filepos, 0);
    fread(&nummappatches, sizeof(int), 1, f);
    lump->data_len = nummappatches;
    lump->data = Malloc(sizeof(char **) * (size_t)nummappatches);

    array = (char **)lump->data;

    wadDebug("readPNames(); #%d\n", lump->data_len);
    for(int i = 0; i < nummappatches; ++i) {
        array[i] = (char *)Malloc(sl);
        fread((void *)array[i], sl, 1, f);
    }
}

void Wad::readSubSectors(void* self, FILE *f, filelump_t *lump) {
    const int length = lump->size / 4;
    auto wad = (Wad*)self;

    lump->data_len = length;
    lump->data = Malloc(sizeof(subsector_t) * length);
    fseek(f, lump->filepos, 0);

    wadDebug("readSubSectors(); #%d\n", lump->data_len);
    for(int i = 0; i < length; i++) {
        subsector_t* subsector = ((subsector_t *)lump->data + i);

        fread(subsector, sizeof(subsector_t), 1, f);
        Wad::subSectorInfo(wad, subsector);
    }
}

void Wad::readNodes(void* self, FILE *f, filelump_t *lump) {
    auto wad = (Wad*)self;
    node_t *node;

    const int length = lump->size / 28;
    lump->data_len = length;
    lump->data = Malloc(sizeof(node_t) * length);
    fseek(f, lump->filepos, 0);

    wadDebug("readNodes(); #%d\n", lump->data_len);
    for(int i = 0; i < length; ++i) {
        node = ((node_t *)lump->data + i);
        fread(&(node->x_part), 2, 1, f);
        fread(&(node->y_part), 2, 1, f);
        fread(&(node->cx), 2, 1, f);
        fread(&(node->cy), 2, 1, f);

        fread(&(node->rbox), 8, 1, f);
        fread(&(node->lbox), 8, 1, f);

        fread(&(node->left), 2, 1, f);
        fread(&(node->right), 2, 1, f);

        Wad::nodeInfo(wad, node);
    }
}

void Wad::readTextures(void* self, FILE *f, filelump_t *lump) {
    fseek(f, lump->filepos, 0);

    auto textures = (texture_t *)Malloc(sizeof(texture_t));
    fread(&(textures->numtextures), 4, 1, f);
    wadDebug("readTextures(); Reading %d textures...\n", textures->numtextures);

    auto offsets = (int *)Malloc(sizeof(int) * (size_t)textures->numtextures);
    fread(offsets, sizeof(int) * textures->numtextures, 1, f);

    textures->mtexture = (maptexture_t *)Malloc(sizeof(maptexture_t) * (size_t)textures->numtextures);
    for(int i = 0; i < textures->numtextures; ++i) {
        fseek(f, lump->filepos + offsets[i], 0);

        auto tex = textures->mtexture + i;
        fread(tex, 22, 1, f);

        if(tex->patchcount <= 0) {
            wadDebug("readTextures(); Texture %s has 0 patches!\n", str8(tex->name));
            tex->patches = NULL;
            continue;
        }

        tex->patches = (mappatch_t *)Malloc(sizeof(mappatch_t) * (size_t)tex->patchcount);
        fread(tex->patches, sizeof(mappatch_t) * tex->patchcount, 1, f);
    }

    Free(offsets);
    lump->data = (texture_t *)textures;
    lump->data_len = 1;
}

/* http://doom.wikia.com/wiki/Palette */
void Wad::readPlaypal(void* self, FILE *f, filelump_t *lump) {
    Wad* w = (Wad*)self;

    if(lump->size > 768) {
        fseek(f, lump->filepos, 0);
        fread(w->m_palette, 768 * sizeof(unsigned char), 1, f);

        wadDebug("readPlaypal(); PLAYPAL palette found.\n");
    } else {
        wadDebug("readPlaypal(); PLAYPAL size < 768! (size: %d).\n", lump->size);
    }
}

void Wad::readColormap(void* self, FILE *f, filelump_t *lump) {
    Wad* w = (Wad*)self;
    int maps_num;

    /* 8704 = 256 * 34 */
    maps_num = lump->size / 256;

    if(maps_num == 34) {
        if(w->m_colormap != NULL) {
            Free(w->m_colormap);
        }

        w->m_colormap = (unsigned char *)Malloc(sizeof(unsigned char) * 8704);
        fseek(f, lump->filepos, 0);
        fread(w->m_colormap, lump->size * sizeof(unsigned char), 1, f);
        wadDebug("readColormap(); COLORMAP found.\n");
    } else {
        wadDebug("readColormap(); maps_num != 34\n");
    }
}


void Wad::freeTexturesLump(texture_t *texture1) {
    int freed_textures = 0;

    for(maptexture_t* texture = texture1->mtexture;
         texture != texture1->mtexture + texture1->numtextures; 
         ++texture) {
        m_logger.msgDebug("Wad::~Wad: Freeing maptexture_t %s(%d patches)...\n",
                str8(texture->name),
                texture->patchcount);

        if(texture->patches) {
            Free(texture->patches);
            texture->patches = NULL;
            freed_textures++;
        }
    }

    if(texture1->mtexture) {
        Free(texture1->mtexture);
        texture1->mtexture = NULL;
    }

    m_logger.msgDebug("Wad::~Wad: Freed %d textures.\n", freed_textures);
}

int Wad::freeImagesArray(std::vector<Image>& images) {
    int freed = 0;

    for(auto& img: images) {
        if(img.m_data) {
            Image::freeImage(&img);
            freed++;
        }
    }

    return freed;
}
void Wad::freeData() {
    filelump_t *lump;

    if((lump = getLump(m_gfxReadFunctions[GL_TEXTURE1].name)) != NULL) {
        if(lump->data != NULL) {
            freeTexturesLump((texture_t *)lump->data);
        }

        freeImagesArray(m_textures1);
    }

    if((lump = getLump(m_gfxReadFunctions[GL_TEXTURE2].name)) != NULL) {
        if(lump->data != NULL) {
            freeTexturesLump((texture_t *)lump->data);
        }

        freeImagesArray(m_textures2);
    }

    if(!m_flats.empty()) {
        int freed_flats = 0;

        for(const auto& flatImg: m_flats) {
            if(flatImg != NULL && flatImg->m_data != NULL) {
                /* m_logger.msgDebug("Freeing m_flats[%d]: %p; flat_img->data: %p\n", */ 
                /*         i, flatImg, flatImg->data); */

                Image::freeImage(flatImg);
                freed_flats++;
            }
        }

        m_flats.clear();

        if(freed_flats > 0) {
            m_logger.msgDebug("Wad::~Wad: %d flats freed.\n", freed_flats);
        }
    }

    char **array;
    int lump_num = 0;

    for(auto& lumpRef: m_dir) {
        auto lump = &lumpRef;

        if(lump->data != NULL) {
            if(strncasecmp(lump->name, m_gfxReadFunctions[GL_PNAMES].name, 8) == 0) {
                m_logger.msgDebug("Wad::~Wad: Freeing %d PNAMES...\n", lump->data_len);
                array = (char **)lump->data;

                for(int i = 0; i < lump->data_len; i++) {
                    Free((void *)(array[i]));
                    array[i] = NULL;
                }
            }
            Free(lump->data);
            lump->data = NULL;
            lump_num++;
        }
        lump_num++;
    }

    if(!m_patches.empty()) {
        int freed_patches = 0;

        for(const auto& imgPtr: m_patches) {
            if(imgPtr != NULL) {
                m_logger.msgDebug("Freeing patches=%p; name='%s'; patch->data=%p;\n", imgPtr, imgPtr->m_name, imgPtr->m_data);

                if(imgPtr->m_data != NULL) {
                    Image::freeImage(imgPtr);
                }

                Free(imgPtr);
                freed_patches++;
            }
        }

        m_patches.clear();

        if(freed_patches > 0) {
            m_logger.msgDebug("Wad::~Wad: %d patches freed.\n", freed_patches);
        }
    }

    m_logger.msgDebug("Wad::~Wad: %d lumps freed.\n", lump_num);
}

void Wad::initData() {
    m_f = NULL;

    m_logger.m_msgLevel = ML_DEBUG;

    m_mapReadFunctions = std::map<MapLump, LumpDef>({
        {ML_THINGS,     {"THINGS",      Wad::readThings}},
        {ML_LINEDEFS,   {"LINEDEFS",    Wad::readLinedefs}},
        {ML_SIDEDEFS,   {"SIDEDEFS",    Wad::readSidedefs}},
        {ML_VERTEXES,   {"VERTEXES",    Wad::readVertexes}},
        {ML_SEGS,       {"SEGS",        Wad::readSegments}},
        {ML_SECTORS,    {"SECTORS",     Wad::readSectors}},
        {ML_SSECTORS,   {"SSECTORS",    Wad::readSubSectors}},
        {ML_NODES,      {"NODES",       Wad::readNodes}}
    });

    m_gfxReadFunctions = std::map<GfxLump, LumpDef>{
        {GL_COLORMAP,   {"COLORMAP",    Wad::readColormap}},
        {GL_PNAMES,     {"PNAMES",      Wad::readPNames}},
        {GL_TEXTURE1,   {"TEXTURE1",    Wad::readTextures}},
        {GL_TEXTURE2,   {"TEXTURE2",    Wad::readTextures}},
        {GL_PLAYPAL,    {"PLAYPAL",     Wad::readPlaypal}}
    };
}

Wad::Wad() {
    initData();
}

Wad::~Wad() {
    freeData();
    close();
}

filelump_t *Wad::getLump(const char *name) {
    auto it = getLumpIterator(name);

    return it == m_dir.end() ? NULL : &*it;
}

std::vector<filelump_t>::iterator Wad::getLumpIterator(const char* name) {
    for(auto it = m_dir.begin(); it != m_dir.end(); ++it) {
        if(strncasecmp(name, (const char *)it->name, 8) == 0) {
            return it;
        }
    }

    m_logger.msgWarning("Wad::getLumpIterator('%s'); Lump not found.\n", str8((char *)name));
    return m_dir.end();
}

filelump_t *Wad::getMapLump(const char *mapName, const char *name) {
    auto mapLumpIt = getLumpIterator(mapName);

    for(auto it = mapLumpIt; it != m_dir.end(); ++it) {
        if(strncasecmp(name, it->name, 8) == 0) {
            return &*it;
        }
    }

    m_logger.msgWarning("Wad::getMapLump(mapName='%s', name='%s'); Lump not found.\n", mapName, name);
    return NULL;
}

bool Wad::isOpen() const {
    return m_f != NULL;
}

bool Wad::open(const char *filename) {
    close();

    m_logger.msgInfo("Wad::open(%s);\n", filename);

    m_filename = filename;
    m_filenameBase = std::string(getField(filename, '/', 0));
    m_f = fopen(filename, "r");

    return isOpen();
}

void Wad::close() {
    m_logger.msgInfo("Wad::close(); f: %p\n", m_f);

    if(isOpen()) {
        fclose(m_f);
        m_f = NULL;
    }

    freeData();
    initData();
}

void Wad::readPatches() {
    filelump_t *lump = getLump(m_gfxReadFunctions[GL_PNAMES].name);

    if(!lump) {
        if(m_info.id[0] == 'I') {
            m_logger.msgWarning("Wad::readPatches(); PNAMES not found!\n");
        }

        return;
    }

    char** pnames = (char **)lump->data;
    m_logger.msgInfo("Wad::readPatches(); Reading %d patches...\n", lump->data_len);

    int missed = 0;

    for(char** pname = pnames; pname != pnames + lump->data_len; ++pname) {
        filelump_t* imgLump = getLump(*pname);

        if(imgLump) {
            Image* patchImg = readImage(m_f, imgLump);

            memset(patchImg->m_name, '\0', 9);
            strncpy(patchImg->m_name, imgLump->name, 8);

            m_patches.push_back(patchImg);
        } else {
            m_patches.push_back(nullptr);
            missed++;
        }
    }

    m_logger.msgInfo("Wad::readPatches(); %d/%d patches found.\n", 
            lump->data_len - missed,
            lump->data_len);
}
    
Image *Wad::getFlat(const char *name) const {
    if(m_flats.empty()) {
        return NULL;
    }

    for(const auto& flatImg: m_flats) {
        if(flatImg && strncasecmp(flatImg->m_name, name, 8) == 0) {
            return flatImg;
        }
    }

    m_logger.msgWarning("getFlat('%s'); Flat not found.\n", name);
    return NULL;
}

int isMarker(char *type, int typeSize, char *str) {
    const char* sep = strchr(str, '_');
    if(sep == NULL) {
        return -1;
    }

    int prefixLen = sep - str;
    prefixLen = prefixLen < typeSize ? prefixLen : typeSize - 1;
    strncpy(type, str, prefixLen);
    type[prefixLen] = '\0';

    if(strcmp(sep + 1, "START") == 0) {
        return 0;
    } else if(strcmp(sep + 1, "END") == 0) {
        return 1;
    } else {
        return -1;
    }
}

void Wad::readFlats() {
    auto f_start = getLumpIterator("F_START");

    char marker[8];
    memset(marker, '\0', 8);

    if(f_start == m_dir.end()) {
        m_logger.msgInfo("Wad::readFlats(); F1_START not found.\n");
        return;
    }

    int len = -1;
    int i = 0;

    for(auto it = f_start; it != m_dir.end(); ++it) {
        auto lump = &*it;

        const int res = isMarker(marker, 8, lump->name);

        if(res != -1) {
            m_logger.msgInfo("readFlats(); Found marker: %s (type=%s)\n", lump->name, marker);
        }

        if(res == 1 && strcmp(marker, "F") == 0) {
            len = i;
            break;
        }

        if(res == -1) {
            i++;
        }
    }

    if(len == 0) {
        m_logger.msgWarning("Wad::readFlats(); F_END not found.\n");
        return;
    }

    m_logger.msgInfo("Wad::readFlats(); Reading %d flats...\n", len);

    auto lump = f_start + 1;
    for(int i = 0; i < len; ) {
        if(isMarker(marker, 8, lump->name) != -1) {
            lump++;
            continue;
        }

        Image* flatImg = readFlat(m_f, &*lump);

        memset(flatImg->m_name, '\0', 9);
        strncpy(flatImg->m_name, lump->name, 8);

        m_flats.push_back(flatImg);

        lump++;
        i++;
    }

    m_logger.msgInfo("Wad::readFlats(); Loaded %d flat textures.\n", m_flats.size());
}

Image *Wad::getTextureFromArray(std::vector<Image>& textures, const char *name) {
    if(name == NULL) {
        return NULL;
    }

    if(name[0] == '-' && !name[1]) {
        return NULL;
    }

    if(textures.size() == 0) {
        m_logger.msgWarning("getTexture('%s'); size == 0!.\n", name);
        return NULL;
    }

    for(auto& tex: textures) {
        if(strncasecmp(name, tex.m_name, 8) == 0) {
            return &tex;
        }
    }

    m_logger.msgWarning("getTexture('%s'); Texture not found.\n",  name);
    return NULL;
}

Image *Wad::getTexture(const char *name) {
    Image* result = getTextureFromArray(m_textures1, name);

    if(result == NULL) {
        result = getTextureFromArray(m_textures2, name); 
        return result;
    }

    return result;
}

void Wad::buildTexturesArray(std::vector<Image>& textures, texture_t *texture_lump) {
    if(texture_lump->numtextures <= 0) {
        return;
    }

    textures.clear();

    const auto patchesSize = m_patches.size();

    for(int i = 0; i < texture_lump->numtextures; ++i) {
        Image texture;

        maptexture_t* tex = texture_lump->mtexture + i;

        m_logger.msgDebug("prepareTextures(); %d; Building '%s'...\n", i, str8(tex->name));

        /* Swap width and height of output texture. Final image will be transposed. */
        texture.m_width  = tex->height;
        texture.m_height = tex->width;

        memset(texture.m_name, '\0', 9);
        strncpy(texture.m_name, tex->name, 8);

        if(tex->patchcount <= 0) {
            m_logger.msgWarning("prepareTextures(); Texture %s has 0 patches.\n", texture.m_name);
            texture.m_data = NULL;
            continue;
        }
        texture.m_data = (unsigned char *)Malloc(sizeof(unsigned char) * (size_t)(tex->width * tex->height));

        unsigned char* tdata = texture.m_data;
        memset(tdata, (unsigned char)247, sizeof(unsigned char) * (tex->width * tex->height));

        for(int j = 0; j < tex->patchcount; ++j) {
            mappatch_t* patch = tex->patches + j;

            if(patch->patch < 0 || patch->patch >= patchesSize) {
                m_logger.msgWarning("prepareTextures(); Texture %s: Reference to non existing patch %d.\n",
                        texture.m_name,
                        patch->patch);
                continue;
            }

            Image* patch_img = m_patches[patch->patch];

            if(patch_img == NULL) {
                m_logger.msgWarning("prepareTextures(); Texture %s: patches[%d] == NULL!\n", 
                        texture.m_name,
                        patch->patch);
                continue;
            }

            unsigned char* pdata = patch_img->m_data;

            const int startX = (patch->originx < 0) ? -patch->originx : 0;
            const int startY = (patch->originy < 0) ? -patch->originy : 0;

            int endX;
            if(patch->originx + patch_img->m_width > tex->width) {
                endX = tex->width - patch->originx;
            } else {
                endX = patch_img->m_width;
            }

            int endY;
            if(patch->originy + patch_img->m_height > tex->height) {
                endY = tex->height - patch->originy;
            } else {
                endY = patch_img->m_height;
            }

            for(int px = startX; px < endX; ++px) {
                for(int py = startY; py < endY; ++py) {
                    const int ppos = (py * patch_img->m_width) + px;

                    const int tpos = ((tex->width - (patch->originx + px) - 1) * tex->height) + (patch->originy + py);

                    if(pdata[ppos] == 247) {
                        continue;
                    }
                    tdata[tpos] = pdata[ppos];
                }
            }
        }

        textures.push_back(texture);
    }
}

void Wad::prepareTexture(std::vector<Image>& textures, Wad* wad, const GfxLump textureLumpId, const char* textureLumpName)
{
    filelump_t* texLump = wad->getLump(m_gfxReadFunctions[textureLumpId].name);

    if(texLump == NULL) {
        m_logger.msgError("%s(); %s not found.\n", __FUNCTION__, textureLumpName);
        return;
    }

    m_logger.msgInfo("%s(); %s found.\n", __FUNCTION__, textureLumpName);
    texture_t* texData = (texture_t *)texLump->data;

    if(texData->numtextures == 0) {
        m_logger.msgWarning("%s(); %s has %d textures.\n", __FUNCTION__, textureLumpName, texData->numtextures);
    }

    buildTexturesArray(textures, texData);
}

void Wad::prepareTextures(Wad *texWad) {
    prepareTexture(m_textures1, texWad, GL_TEXTURE1, m_gfxReadFunctions[GL_TEXTURE1].name);
    prepareTexture(m_textures2, texWad, GL_TEXTURE2, m_gfxReadFunctions[GL_TEXTURE2].name);
}

static void fillSideDefInfo(sidedef_info_t* info, Wad* texWad, const line_t* line, const sidedef_t* side, const sector_t* sector) {
    if(side == NULL) {
        info->uTex = NULL;
        info->mTex = NULL;
        info->lTex = NULL;
        return;
    }

    info->uTex = texWad->getTexture(side->uppertex);
    info->mTex = texWad->getTexture(side->middletex);
    info->lTex = texWad->getTexture(side->lowertex);

    if(sector == NULL) {
        info->fTex = NULL;
        info->cTex = NULL;
    } else {
        info->fTex = texWad->getFlat(sector->floortex);
        info->cTex = texWad->getFlat(sector->ceiltex);
    }
}

void Wad::findTextures(Wad *texwad) {
    for(auto& line: m_lines) {
        fillSideDefInfo(&(line.front), texwad, &line, line.rside, line.rsector);
        fillSideDefInfo(&(line.back), texwad, &line, line.lside, line.lsector);
    }
}

void Wad::prepareLines(const char *mapName) {
    std::map<MapLump, filelump_t*> lumps = {
        {ML_LINEDEFS, nullptr},
        {ML_VERTEXES, nullptr},
        {ML_SIDEDEFS, nullptr},
        {ML_SECTORS, nullptr}
    };
    
    for(const auto &lump: lumps) {
        auto key = lump.first;
        const auto name = m_mapReadFunctions.at(key).name;

        lumps[key] = getMapLump(mapName, name);

        assert(lumps[key]);
    }

    auto linedefs = (linedef_t *)lumps[ML_LINEDEFS]->data;
    auto vertexes = (vertex_t  *)lumps[ML_VERTEXES]->data;
    auto sidedefs = (sidedef_t *)lumps[ML_SIDEDEFS]->data;
    auto sectors  = (sector_t  *)lumps[ML_SECTORS]->data;

    assert(linedefs);
    assert(vertexes);
    assert(sidedefs);
    assert(sectors);

    filelump_t* l_linedefs = lumps[ML_LINEDEFS];
    m_logger.msgInfo("Wad::prepareLines(%s); Allocating %d lines...\n", mapName, l_linedefs->data_len);

    m_lines.clear();
    m_lines.reserve(l_linedefs->data_len);

    for(int i = 0; i < l_linedefs->data_len; ++i) {
        line_t line;

        line.inFront = false;
        line.linedef = linedefs + i;

        line.va = vertexes + linedefs[i].start;
        line.vb = vertexes + linedefs[i].end;

        line.lside = NULL;
        line.rside = NULL;

        if(linedefs[i].leftsdef != (short)-1) {
            line.lside = sidedefs + linedefs[i].leftsdef;
        }
        if(linedefs[i].rightsdef != (short)-1) {
            line.rside = sidedefs + linedefs[i].rightsdef;
        }

        line.lsector = NULL;
        line.rsector = NULL;

        if(line.lside != NULL) {
            line.lsector = sectors + line.lside->sector;
        }
        if(line.rside != NULL) {
            line.rsector = sectors + line.rside->sector;
        }

        m_logger.msgDebug("Extracted line %d: (A, B) = (%d, %d); Sides: (L, R): (%d, %d); Sectors: (%d, %d)\n",
                i,
                linedefs[i].start,
                linedefs[i].end,
                linedefs[i].leftsdef,
                linedefs[i].rightsdef,
                line.lside == NULL ? -1 : line.lside->sector,
                line.rside == NULL ? -1 : line.rside->sector);

        m_lines.push_back(line);
    }
}

int Wad::read() {
    const int BUFSZ = 5;
    char buf[BUFSZ];

    m_logger.msgInfo("Wad::read('%s'); f=%p\n", m_filename.c_str(), m_f);
    if(!isOpen()) {
        return 1;
    }

    fread(buf, 1, 4, m_f);
    if(strncmp(buf + 1, "WAD", 3) != 0) {
        m_logger.msgError("Wad::read(): %s is not WAD file.\n", m_filename.c_str());
        return 1;
    }

    strncpy(m_info.id, buf, 4);

    fread(&(m_info.numlumps), 4, 1, m_f);
    fread(&(m_info.infotableofs), 4, 1, m_f);

    fseek(m_f, m_info.infotableofs, 0);

    m_logger.msgDebug("Wad::read: Processing %d directory indexes...\n", m_info.numlumps);

    m_dir.clear();
    m_dir.reserve(m_info.numlumps);

    for(int dirIdx = 0; dirIdx < m_info.numlumps; ++dirIdx) {
        filelump_t lump;

        fread(&(lump.filepos), 4, 1, m_f);
        fread(&(lump.size), 4, 1, m_f);
        fread(lump.name, 8, 1, m_f);

        lump.data = NULL;
        lump.data_len = 0;

        m_logger.msgDebug("[%4d] [%10s] POS: %4d SIZE: %d\n",
                dirIdx,
                str8(lump.name),
                lump.filepos,
                lump.size);

        m_dir.push_back(lump);
    }

    for(auto& lumpRef: m_dir) {
        auto lump = &lumpRef;

        if(isMapName(lump->name)) {
            m_logger.msgInfo("Wad::read(); Reading map %s...\n", lump->name);
            continue;
        }

        bool found = false;

        for(const auto& it: m_mapReadFunctions) {
            if(strncasecmp(it.second.name, lump->name, 8) == 0) {
                it.second.func(this, m_f, lump);
                found = true;
                break;
            }
        }

        if(found) {
            continue;
        }

        for(const auto& it: m_gfxReadFunctions) {
            if(strncasecmp(it.second.name, lump->name, 8) == 0) {
                it.second.func(this, m_f, lump);
                found = true;
                break;
            }
        }

        if(found) {
            continue;
        }
    }

    readPatches();
    readFlats();

    /* listContent(); */

    return 0;
}

const thing_t* Wad::getPlayer(const char* map) {
    filelump_t* things = getMapLump(map, "THINGS");
    const char* msgPrefix = "Wad::getPlayer";

    if(things == NULL) {
        printf("%s; things == NULL\n", msgPrefix);
        return NULL;
    }

    const int things_len = things->data_len;
    const thing_t* player = getThingByType((thing_t *)things->data, things_len, 1);

    if(player == NULL) {
        printf("%s: unable to find player for map '%s'.\n", msgPrefix, map);
    }

    return player;
}
