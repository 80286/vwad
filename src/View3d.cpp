/* * Rendering based on algorithm from Eureka doom editor (http://eureka-editor.sourceforge.net) */
#include "View3d.h"
#include <algorithm>

/* Image of missing texture (exclamation mark): */
static unsigned char noTextureImageData[] = {
/*  0    1    2    3    4    5    6    7    */
    0  , 0  , 0  , 0  , 0  , 0  , 0  , 0,
    0  , 0  , 0  , 0  , 0  , 0  , 0  , 0,
    0  , 0  , 0  , 0  , 0  , 0  , 0  , 0,
 
    0  , 32 , 32 , 32 , 32 , 0  , 32 , 0,
    0  , 32 , 32 , 32 , 32 , 0  , 32 , 0,
 
    0  , 0  , 0  , 0  , 0  , 0  , 0  , 0,
    0  , 0  , 0  , 0  , 0  , 0  , 0  , 0,
    0  , 0  , 0  , 0  , 0  , 0  , 0  , 0
};

static unsigned char skyColor = 106;
static unsigned char skyImageData[] = {
    skyColor
};

static Image noTextureImage(8, 8, noTextureImageData, "NONE");
static Image skyImage(1, 1, skyImageData, "SKY");

/* Angle of left clipping plane: */
static const float leftClip  = R_135;
/* Angle of right clipping plane. */
static const float rightClip = R_45;

static const float NEAR_PLANE = 2.0f;

/* Converts pair (inverted_z, sectorHeight) to y screen coord. */
static int inverseDistanceToY(int screenHeight, int viewHeight, double iz, int sectorHeight) {
    const float height = sectorHeight - viewHeight;

    /* height * iz * NEAR_PLANE == (height * NEAR_PLANE) / z */
    int y = (height * iz * NEAR_PLANE) * (float)screenHeight;

    /* [-screenHeight, screenHeight] -> [0, screenHeight] */
    y = (screenHeight - y) / 2;

    return y;
}

static float screenYToDistance(int screenHeight, int viewHeight, int y, int sectorHeight) {
    int sech;

    y = screenHeight - 2 * y;
    sech = sectorHeight - viewHeight;

    return ((float)sech * NEAR_PLANE * (float)screenHeight) / (float)y;
}

/**************************************************
 * iz1 = 1/y1
 * iz2 = 1/y2
 *
 * If (y1 < y2) => 
 * => (1/y2  < 1/y1)
 * => (1/y1  > 1/y2)
 * =>  iz1   > iz2
 *
 *************************************************/
static inline bool izCloser(double iz1, double iz2) {
    return iz1 > iz2;
}

static inline bool middleDistanceCmp(const S_Wall *wall1, const S_Wall *wall2) {
    return izCloser(wall1->middleIz, wall2->middleIz);
}

/**************************************************
 * pointOnSide();
 *
 * @param A = (ax, ay)
 * @param B = (bx, by)
 * @param C = (cx, cy)
 *
 * @return (C - A) x (B - A)
 *************************************************/
static inline int pointOnSide(int ax, int ay, int bx, int by, int cx, int cy) {
    return (cx - ax) * (by - ay) - (cy - ay) * (bx - ax);
}

struct DistanceCmpClass {
    const ViewPoint* m_view = nullptr;

    bool operator() (const S_Wall* wall1, const S_Wall* wall2) {
        if(fabs(wall1->t_iz - wall2->t_iz) >= EPS) {
            return izCloser(wall1->t_iz, wall2->t_iz);
        }

        const line_t* l1 = wall1->line;
        const line_t* l2 = wall2->line;

        vertex_t* v1 = NULL;

        if(l1->va == l2->va || l1->va == l2->vb) {
            v1 = l1->vb;
        } else if(l1->vb == l2->va || l1->vb == l2->vb) {
            v1 = l1->va;
        }

        if(v1 == NULL) {
            return izCloser(wall1->middleIz, wall2->middleIz);
        }

        assert(m_view);

        const vertex_t* v2a = l2->va;
        const vertex_t* v2b = l2->vb;

        const int vSide = pointOnSide(v2a->x, v2a->y, v2b->x, v2b->y, v1->x, v1->y);
        const int cSide = pointOnSide(v2a->x, v2a->y, v2b->x, v2b->y, m_view->pos_x, m_view->pos_y);

        return (vSide >= 0 && cSide >= 0) || (vSide <= 0 && cSide <= 0);
    }
};

static inline bool screenXCmp(const S_Wall *wall1, const S_Wall *wall2) {
    return wall1->s_x1 < wall2->s_x1;
}

int View3d::angleToScreenX(const float x) const {
    return angleToX(m_canvas->getWidth(), x);
}

int View3d::distanceToScreenY(const double iz, const int height) const {
    return inverseDistanceToY(m_canvas->getHeight(), m_viewPoint->pos_z, iz, height);
}

float View3d::YToDistance(const int y, const int height) const {
    return screenYToDistance(m_canvas->getHeight(), m_viewPoint->pos_z, y, height);
}

void View3d::setIWAD(Wad* iwad) {
    m_iwad = iwad;
}

void View3d::setPWAD(Wad* pwad) {
    m_pwad = pwad;
}

View3d::View3d(Canvas* canvas) {
    m_canvas = canvas;
    m_pwad = NULL;
    m_iwad = NULL;

    m_zBuffer = NULL;
    m_zBufferLength = m_canvas->getWidth();

    m_draggedRight = false;

    m_cfgLights = true;
    m_cfgDetailLevel = 1;

    m_viewPoint = nullptr;
}

View3d::~View3d() {
    clearVisibleWalls();
}

void View3d::clearVisibleWalls() {
    for(auto wallPtr: m_visibleWalls) {
        delete wallPtr;
    }

    m_visibleWalls.clear();
}

float getIntersectY(float ax, float ay, float segmentSlope, float lineSlope) {
    const float b = ay - segmentSlope * ax;
    const float x = -b / (segmentSlope - lineSlope);
    const float y = lineSlope * x;

    return y;
}

void View3d::selectVisibleWalls(std::vector<line_t>& lines) {
    m_visibleWalls.clear();

    int i = -1;
    for(auto& line: lines) {
        ++i;

        line.inFront = false;

        const auto ax = (float)line.va->x;
        const auto ay = (float)line.va->y;
        const auto bx = (float)line.vb->x;
        const auto by = (float)line.vb->y;

        const auto ax_t = ax - (float)m_viewPoint->pos_x;
        const auto ay_t = ay - (float)m_viewPoint->pos_y;
        const auto bx_t = bx - (float)m_viewPoint->pos_x;
        const auto by_t = by - (float)m_viewPoint->pos_y;

        /* 
         * x_r = cos(PI/2 - t) * x - sin(PI/2 - t) * y = sin(T) * x - cos(T) * y
         * y_r = sin(PI/2 - t) * x + cos(PI/2 - t) * y = cos(T) * x + sin(T) * y
         */
        const auto ax_r = ax_t * m_viewPoint->rot_sin - ay_t * m_viewPoint->rot_cos;
        const auto ay_r = ax_t * m_viewPoint->rot_cos + ay_t * m_viewPoint->rot_sin;
        const auto bx_r = bx_t * m_viewPoint->rot_sin - by_t * m_viewPoint->rot_cos;
        const auto by_r = bx_t * m_viewPoint->rot_cos + by_t * m_viewPoint->rot_sin;

        if(ay_r <= 0.1f && by_r <= 0.1f) {
            continue;
        }

        float angle1 = pointToAngle(ax_r, ay_r);
        float angle2 = pointToAngle(bx_r, by_r);

        const float lineSpan = rad(angle1 - angle2);

        // Default side: right side
        int side = 0;

        if(lineSpan >= R_180) {
            // left side
            side = 1;

            if(line.lside == NULL) {
                continue;
            }

            const float tmp = angle1;
            angle1 = angle2;
            angle2 = tmp;
        } else {
            if(line.rside == NULL) {
                continue;
            }
        }

        /* Save unclipped angles: */
        float rang1 = angle1;
        float rang2 = angle2;

        /* Coords of vector parallel to (A - B) vector: */
        const float spanX = ax_r - bx_r;
        const float spanY = ay_r - by_r;

        float normal;
        if(side == 0) {
            normal = pointToAngle(-spanY,  spanX);
        } else {
            normal = pointToAngle( spanY, -spanX);
        }

        /* Clip angles to the FOV: */
        angle1 = rad(angle1);
        angle2 = rad(angle2);

        if(angle1 > leftClip) { // a1 > 135
            if(angle2 > leftClip) { // a2 > 135
                /* 2 vertices on the left side: */
                continue;
            }

            // a1 > 135 && a2 <= 135, a1 > a2: ok
            angle1 = leftClip;
        }

        if(angle2 < rightClip || angle2 > R_225) { // a2 < 45
            if(angle1 < rightClip) { // a1 < 45
                /* 2 vertices on the right side: */
                continue;
            }

            angle2 = rightClip;
            if(rang2 > R_225) {
                rang2 -= R_360;
            }
        }

        const int s_x1 = angleToScreenX(angle1);
        const int s_x2 = angleToScreenX(angle2);

        if(s_x1 > s_x2) {
            printf("[%d] s_x1 > s_x2;\n", i);
            continue;
        }

        /* Length of wall: */
        const float wlen = sqrtf(spanX * spanX + spanY * spanY);

        /* 
         * Height of parallelogram built on A, B vectors:
         * A(ax, ay);
         * B(bx, by);
         * wlen = |A - B|
         *
         * P_r = |A x B| = |A| * |B| * sin(angle between A & B vectors) =
         * = |A| * |B| * sin(z);
         *
         * sin(x - y) = sin(x) * cos(y) - cos(x) * sin(y);
         *
         * For AB segment:
         * sin(z) = sin(angle1) * cos(angle2) - cos(angle1) * sin(angle2) =
         * = (ay / |A|) * (bx / |B|) - (ax / |A|) * (by / |B|);
         *
         * |A x B| = |A| * |B| * sin(z) = 
         * = |A| * |B| ( (ay / |A|) * (bx / |B|) - (ax / |A|) * (by / |B|) ) =
         * = ay * bx - ax * by = dist
         *
         * Because |A x B| (cross product of A and B) is interpreted as area of parallelogram built on A & B vectors:
         *      P_r = wlen * dist = a * h
         * so height of parallelogram is equal to:
         *      dist = P_r / wlen
         *
         * */
        const float dist = fabs(((by_r * ax_r) - (bx_r * ay_r)) / wlen);

        int screenSpan = s_x2 - s_x1;
        screenSpan = (screenSpan == 0) ? 1 : screenSpan;

        /*
         * Inverted distance (iz - inverted Z value) is calculated for each vertex of segment.
         * If vertex lies inside viewing frustum then it's simply equal to 1/y.
         *
         * Expression iz = cos(normal - angle) / sin(angle1) / dist
         * is applied only for angles from FOV range [135, 45] ([0.75 * M_PI, 0.25 * M_PI])
         *
         * That's why we're clipping angles tied to vertices instead of calculating
         * intersection between segment and clipping lines.
         *
         */
        double iz1 = ((double)cosf(angle1 - normal - R_180) / (double)sinf(angle1)) / (double)dist;
        double iz2 = ((double)cosf(normal - R_180 - angle2) / (double)sinf(angle2)) / (double)dist;
        iz1 = fabs(iz1);
        iz2 = fabs(iz2);

        /* Increase of iz between left and right vertices: */
        double deltaIz = (iz2 - iz1) / (double)screenSpan;

        auto screenWall = new S_Wall;

        screenWall->iz1 = iz1;
        screenWall->iz2 = iz2;
        screenWall->deltaIz = deltaIz;
        screenWall->middleIz = iz1 + ((double)screenSpan / 2) * deltaIz;
        screenWall->angle1 = angle1;
        screenWall->angle2 = angle2;
        screenWall->deltaAngle = (angle2 - angle1) / (float)screenSpan;
        screenWall->rangle1 = rang1;
        screenWall->rangle2 = rang2;
        screenWall->deltaLength = wlen / (rang1 - rang2);
        screenWall->deltaXoffset = (screenWall->deltaAngle * screenWall->deltaLength);
        screenWall->s_x1 = s_x1;
        screenWall->s_x2 = s_x2;
        screenWall->wlen = wlen;
        screenWall->side = side;
        screenWall->line = &line;
        screenWall->i = i;

        m_visibleWalls.push_back(screenWall);
    }
}

enum {
    SS_INVISIBLE  = 0x00,
    SS_FLOOR      = 0x01,
    SS_MIDDLE     = 0x02,
    SS_CEIL       = 0x04,
    SS_LOWER      = 0x08,
    SS_UPPER      = 0x10,
    SS_ABOVE      = (SS_CEIL  | SS_UPPER),
    SS_BELOW      = (SS_FLOOR | SS_LOWER),
    SS_FLAT       = (SS_FLOOR | SS_CEIL),
    SS_WALL       = (SS_LOWER | SS_UPPER | SS_MIDDLE)
};

enum {
    SS_UNPEGGED_UPPER = 0x08,
    SS_UNPEGGED_LOWER = 0x10
};

S_Surface::S_Surface() : tex(NULL), texHeight(0), flags(0) {
}

void S_Wall::init() {
    t_angle = angle1;
    t_iz = iz1;
    t_xoffset = wlen - (rangle1 - angle1) * deltaLength;
}

void S_Wall::step() {
    t_angle += deltaAngle;
    t_iz += deltaIz;
    t_xoffset += deltaXoffset;
}

void S_Wall::calcSpan(int x) {
    /* Current x span: */
    const float xw = (float)(x - s_x1);

    /* Angle for current x span interpolated between [angle1, angle2]: */
    t_angle = angle1 + xw * deltaAngle;

    /* Inverted z value interpolated between [iz1, iz2]: */
    t_iz = iz1 + (double)xw * deltaIz;

    /* Angle between unclipped angles: */
    t_rangle = rangle1 - t_angle;

    /* Initial x offset of texture: */
    t_xoffset = (t_rangle * deltaLength);
}

inline Image* getTex(Image* tex) {
    return tex == NULL ? &noTextureImage : tex;
}

Image* g_skyTex = NULL;

inline bool isSky(Image* tex) {
    return (tex == g_skyTex);
}

void unpeggSurfaceTex(S_Surface* s) {
    if(s->tex) {
        s->y_offset -= ((s->height1 - s->height2) % s->tex->m_width);
    }
}

void S_Wall::selectSurfaces(int viewHeight) {
    sector_t  *frontSector;
    sidedef_t *frontSide;
    sector_t  *backSector;
    sidedef_info_t *frontInfo;
    sidedef_info_t *backInfo;

    if(side == 0) {
        frontSector = line->rsector;
        backSector  = line->lsector;

        frontSide = line->rside;
        frontInfo = &(line->front);
        backInfo  = &(line->back);
    } else {
        frontSector = line->lsector;
        backSector  = line->rsector;

        frontSide = line->lside;
        frontInfo = &(line->back);
        backInfo  = &(line->front);
    }

    if(frontSector->floorheight < viewHeight) {
        floor.height2   = -9999;
        floor.height1   = frontSector->floorheight;
        floor.texHeight = floor.height1;
        floor.tex       = isSky(frontInfo->fTex) ? &skyImage : getTex(frontInfo->fTex);
        floor.flags     = SS_FLOOR;
        floor.light     = frontSector->lightlevel;
        floor.light     = floor.tex == &skyImage ? 255 : frontSector->lightlevel;
    }

    if(frontSector->ceilheight > viewHeight) {
        ceil.height2   = frontSector->ceilheight;
        ceil.height1   = 9999;
        ceil.texHeight = ceil.height2;
        ceil.tex       = isSky(frontInfo->cTex) ? &skyImage : getTex(frontInfo->cTex);
        ceil.flags     = SS_CEIL;
        ceil.light     = ceil.tex == &skyImage ? 255 : frontSector->lightlevel;
    }

    if(backSector != NULL) {
        if(frontSector->floorheight < backSector->floorheight) {
            lower.height2  = frontSector->floorheight;
            lower.height1  = backSector->floorheight;
            lower.tex      = isSky(frontInfo->fTex) && isSky(backInfo->fTex) ? &skyImage : getTex(frontInfo->lTex);
            lower.flags    = SS_LOWER;
            lower.x_offset = frontSide->x;
            lower.y_offset = frontSide->y;
            if(line->linedef->flags & SS_UNPEGGED_LOWER) {
                unpeggSurfaceTex(&(lower));
            }
            lower.light    = frontSector->lightlevel;
        }

        if(frontSector->ceilheight > backSector->ceilheight) {
            upper.height2  = backSector->ceilheight;
            upper.height1  = frontSector->ceilheight;
            upper.tex      = isSky(frontInfo->cTex) && isSky(backInfo->cTex) ? &skyImage : getTex(frontInfo->uTex);
            upper.flags    = SS_UPPER;
            upper.x_offset = frontSide->x;
            upper.y_offset = frontSide->y;
            upper.light    = frontSector->lightlevel;
            if(!(line->linedef->flags & SS_UNPEGGED_UPPER)) {
                unpeggSurfaceTex(&(upper));
            }
        }
    } else {
        middle.height2   = frontSector->floorheight;
        middle.height1   = frontSector->ceilheight;
        middle.tex       = getTex(frontInfo->mTex);
        middle.flags     = SS_MIDDLE | SS_FLOOR | SS_CEIL;
        middle.x_offset  = frontSide->x;
        middle.y_offset  = frontSide->y;
        middle.light     = frontSector->lightlevel;
        if(line->linedef->flags & SS_UNPEGGED_LOWER) {
            unpeggSurfaceTex(&(middle));
        }
    }
}

void View3d::clipOneSidedWalls() {
    for(int i = 0; i < m_visibleWalls.size(); ++i) {
        S_Wall* wall = m_visibleWalls[i];

        /* Initial value of visible columns: */
        int visibleColumns = wall->s_x2 - wall->s_x1;

        /* Initial inverted-z value: */
        double iz = wall->iz1;

        const bool oneSided = wall->line->isOneSided();

        for(int x = wall->s_x1; x < wall->s_x2; ++x) {
            if(izCloser(m_zBuffer[x], iz)) {
                visibleColumns--;
            } else if(oneSided) {
                m_zBuffer[x] = iz;
            }

            iz += wall->deltaIz;
        }

        if(visibleColumns == 0) {
            delete m_visibleWalls[i];
            m_visibleWalls[i] = NULL;
        } else {
            wall->selectSurfaces(m_viewPoint->pos_z);
        }
    }

    /* Remove null pointers: */
    auto it = m_visibleWalls.begin();
    while(it != m_visibleWalls.end()) {
        if(*it == NULL) {
            it = m_visibleWalls.erase(it);
        } else {
            ++it;
        }
    }
}

static inline unsigned char remapColor(Wad* iwad, unsigned char color, int light) {
    /* light \in [0, 255]
     * light >> 3 \in [0, 31] */

    int map = (unsigned char)(light >> 3);
    map = 31 - map;
    map = map < 0 ? map : (map > 31 ? 31: map);

    return iwad->getColormapIndex((unsigned char)map, color);
}

void View3d::drawFlatColumn(S_Surface *surface, int x, int y1, int y2) {
    const int screenWidth = m_canvas->getWidth();

    const float xang = xToAngle(x, screenWidth);
    const float cang = m_viewPoint->rotate + (xang - R_90);
    /* 
     * xang(wall->t_angle) \in [R_135, R_45]
     *
     * modv = 1/sin(xang);
     *
     * modv(wall->t_modv) wydłuża aktualny promień tak, aby zredukować łuk okręgu
     * do boku kwadratu, w którym wpisany jest ten okrąg.
     */

    const float modv = 1.0f / sinf(xang);
    const float cosv = cosf(cang) * modv;
    const float sinv = sinf(cang) * modv;

    uint8_t* texPtr = surface->tex->m_data;
    const int texWidth = surface->tex->m_width;
    const int texHeight = surface->tex->m_height;

    const auto bpp = m_canvas->getBytesPerPixel();
    uint8_t* screenPtr = m_canvas->m_data + bpp * (y1 * screenWidth + x);

    for(int y = y1; y < y2; ++y) {
        const float dist = YToDistance(y, surface->texHeight);

        /* Equation of a circle: */
        int texX = (m_viewPoint->pos_x + (int)(cosv * dist));
        int texY = (m_viewPoint->pos_y + (int)(sinv * dist));

        texX = texX & (texWidth - 1);
        texY = texY & (texHeight - 1);

        uint8_t* tex = texPtr + (texY * texWidth) + texX;

        const uint8_t palleteIndex = m_cfgLights ? remapColor(m_iwad, *tex, surface->light) : *tex;

        m_iwad->paletteIndexToRGB(palleteIndex, screenPtr);

        screenPtr += (bpp * screenWidth);
    }
}

void View3d::drawWallColumn(S_Wall* wall, S_Surface* surface, int x, int y1, int y2, int ry1, int ry2) {
    uint8_t* screen = m_canvas->m_data;
    const int screenW = m_canvas->getWidth();

    uint8_t* tex = surface->tex->m_data;
    const int texHeight = surface->tex->m_height;
    const int texWidth = surface->tex->m_width;

    /*
     * rangle1, rangle2:    angles before clipping to FOV
     * angle1, angle2:      values clipped to FOV
     * wlen:                not clipped width of wall 
     *
     * wall->deltaAngle: increase of angle between angle1 i angle2
     *      deltaAngle = (angle1 - angle2) / (wall->s_x2 - wall->s_x1)
     *
     * wall->deltaLength: increase of wall of length between vertices:
     *      deltaLength = wlen / (wall->rangle1 - wall->rangle2)
     *
     * ang: angle for current screen x column
     *      ang = angle1 + (x - s_x1) * deltaAngle \in [angle1, angle2]
     *      ang = (rangle1 - ang) \in [0, rangle1 - rangle2]
     *      =>
     *      ang * deltaLength = 
     *      t * wlen = // t \in [0, 1]
     *      = (ang / (rangle1 - rangle2)) * wlen;
     *
     * */

    /* Increase of height from lower to upper point (before clipping [y1, y2] to [0, scr_h]): */
    const float deltaHeight = ((float)(surface->height1 - surface->height2) / (float)(ry2 - ry1));

    /* When upper edge is above screen then calc initial shift: */
    float texY = (float)(y1 - ry1) * deltaHeight + (float)surface->y_offset;
    texY = modf(texY, (float)texHeight);

    const auto bpp = m_canvas->getBytesPerPixel();

    float texX = wall->t_xoffset + surface->x_offset;
    texX = modf(texX, (float)texHeight);

    uint8_t* screenPtr = screen + bpp * (y1 * screenW + x);
    uint8_t* texStart = tex + (int)texX * texWidth;
    uint8_t* texPtr = texStart;

    for(int y = y1; y < y2; ++y) {
        const uint8_t palleteIndex = m_cfgLights ? remapColor(m_iwad, *texPtr, surface->light) : *texPtr;

        m_iwad->paletteIndexToRGB(palleteIndex, screenPtr);

        screenPtr += (bpp * screenW);
        texY += deltaHeight;
        texPtr = texStart + ((int)texY % texWidth);
    }
}

void View3d::drawWallSurface(S_Wall *wall, S_Surface *surface, int x) {
    if(surface->flags == 0) {
        return;
    }

    /* Convert (1/y, h) to screen y: */
    const int ry1 = distanceToScreenY(wall->t_iz, surface->height1);
    const int ry2 = distanceToScreenY(wall->t_iz, surface->height2);

    int y1 = ry1;
    int y2 = ry2;

    /* Clip screen y to available y screen space: */
    if(y1 < m_oy1) {
        y1 = m_oy1;
    }
    if(y2 > m_oy2) {
        y2 = m_oy2;
    }

    if(y1 > y2) {
        return;
    }
    
    /*
     * Available y screen space:
     * m_oy1: 0
     * m_oy2: h - 1
     *
     * y1, y2:
     *      0 < y1 < y2 < h - 1;
     */

    if((surface->flags & SS_ABOVE) && y2 > m_oy1) {
        m_oy1 = y2;
    }
    if((surface->flags & SS_BELOW) && y1 < m_oy2) {
        m_oy2 = y1;
    }

    if(surface->flags & SS_WALL) { 
        drawWallColumn(wall, surface, x, y1, y2, ry1, ry2);
    } else if(surface->flags & SS_FLAT) {
        drawFlatColumn(surface, x, y1, y2);
    }
}

static void img_copyColumn(uint8_t *img, int width, int height, int x, int c, const int bpp) {
    if(c == 0) return;

    const int step = bpp * width;
    uint8_t* src = img + bpp * x;

    for(int y = 0; y < height; ++y) {
        uint8_t* dst = src + bpp;

        for(int j = 0; j < (c - 1); ++j) {
            dst[0] = src[0];
            dst[1] = src[1];
            dst[2] = src[2];
            dst += bpp;
        }

        src += step;
    }
}

void View3d::drawColumns() {
    std::vector<S_Wall*> stack;

    std::sort(m_visibleWalls.begin(), m_visibleWalls.end(), screenXCmp);

    const int screenWidth  = m_canvas->getWidth();
    const int screenHeight = m_canvas->getHeight();
    const int x_step = m_cfgDetailLevel;

    for(int x = 0; x < screenWidth; x += x_step) {
        m_oy1 = 0;
        m_oy2 = screenHeight - 1;

        stack.clear();

        for(auto it = m_visibleWalls.begin(); it != m_visibleWalls.end(); ++it) {
            auto wall = *it;

            if(wall == NULL) {
                continue;
            }

            if(x >= wall->s_x2) {
                delete wall;
                (*it) = NULL;
                continue;
            }

            if(x < wall->s_x1) {
                continue;
            }

            if(x_step != 1) {
                wall->calcSpan(x);
            } else {
                if(x == wall->s_x1) {
                    wall->init();
                } else {
                    wall->step();
                }
            }
            stack.push_back(wall);
        }

        DistanceCmpClass cmpObj;
        cmpObj.m_view = m_viewPoint;
        std::sort(stack.begin(), stack.end(), cmpObj);

        for(auto wall: stack) {
            if(m_oy1 >= m_oy2) {
                break;
            }

            if(wall->line->isOneSided()) {
                drawWallSurface(wall, &(wall->floor),  x);
                drawWallSurface(wall, &(wall->ceil),   x);
                drawWallSurface(wall, &(wall->middle), x);
            } else {
                drawWallSurface(wall, &(wall->floor),  x);
                drawWallSurface(wall, &(wall->ceil),   x);
                drawWallSurface(wall, &(wall->lower),  x);
                drawWallSurface(wall, &(wall->upper),  x);
            }
        }

        img_copyColumn(m_canvas->m_data, screenWidth, screenHeight, x, x_step, m_canvas->getBytesPerPixel());
    }
}

void View3d::draw(const ViewPoint* viewPoint) {
    m_viewPoint = viewPoint;
    m_canvas->clear();

    if(m_pwad == NULL) {
        printf("View3d::draw(); pwad == NULL\n");
        return;
    }

    auto& lines = m_pwad->GetLines();

    if(lines.empty()) {
        printf("View3d::draw(); pwad->m_lines is empty\n");
        return;
    }

    /* printf("==================================================\n"); */
    /* printf("View3d::draw();\n\tScreen (%d, %d);\n\tView (%d, %d, %d);\n\tRotate: (rad: %.2f, ang: %d);\n", */ 
    /*         canvas->getWidth(), */
    /*         canvas->getHeight(), */
    /*         view.pos_x, */
    /*         view.pos_y, */
    /*         view.pos_z, */
    /*         view.rotate, */
    /*         radToDeg(view.rotate)); */

    if(m_zBuffer == NULL) {
        if(m_canvas->getWidth() != m_zBufferLength) {
            delete m_zBuffer;
            m_zBufferLength = m_canvas->getWidth();
        }

        m_zBuffer = new double[m_zBufferLength];
    }

    for(int i = 0; i < m_zBufferLength; ++i) {
        m_zBuffer[i] = 0.0;
    }

    g_skyTex = m_iwad->getFlat((char *)"F_SKY1");

    selectVisibleWalls(lines);

    std::sort(m_visibleWalls.begin(), m_visibleWalls.end(), middleDistanceCmp);

    clipOneSidedWalls();
    
    drawColumns();
}

void View3d::toggleDetailsLevel() {
    m_cfgDetailLevel = m_cfgDetailLevel < 5 ? m_cfgDetailLevel + 1 : 1;
}

void View3d::toggleLighting() {
    m_cfgLights = !m_cfgLights;
}
