#ifndef SDLAPP_H
#define SDLAPP_H

#include "LoadSettings.h"
#include "SdlBaseApp.h"
#include "ViewController.h"

class SdlApp : public SdlBaseApp {
public:
    SdlApp(const int width, const int height);
    ~SdlApp();

    void Init(const LoadSettings& settings);
    void BeginLoop();

private:
    ViewController* m_viewController;
};

#endif
