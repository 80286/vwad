#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H

#include "Constants.h"
#include "Point.h"

class InputHandler {
public:
    virtual void handleKeyPress(const KeyConstant key) = 0;
    virtual void handleMousePress(const Point* point, const MouseConstant mouseButton) = 0;
    virtual void handleMouseMove(const Point* point) = 0;
    virtual void handleMouseRelease(const Point* point, const MouseConstant mouseButton) = 0;
    virtual void handleMouseWheel(int delta) = 0;
};


#endif
