#include "ViewPoint.h"
#include "Constants.h"
#include <cmath>

static float moveStep = 24.0f;
static float mouseRotateStep = R_180 / 72.0f;

ViewPoint::ViewPoint(void) {
    pos_x = 0;
    pos_y = 0;
    pos_z = 48;
    setRotate(0.0f);
}

void ViewPoint::setPos(int x, int y) {
    pos_x = x;
    pos_y = y;
}

void ViewPoint::setPos(int x, int y, int z) {
    setPos(x, y);
    pos_z = z;
}

void ViewPoint::setRotate(float value) {
    rotate = value;
    rot_sin = sinf(rotate);
    rot_cos = cosf(rotate);

    dir_x = (int)(moveStep * rot_cos);
    dir_y = (int)(moveStep * rot_sin);
}

void ViewPoint::turnLeft(float step) {
    float r = rotate + step;

    if(r >= R_360) {
        r -= R_360;
    }

    setRotate(r);
}

void ViewPoint::turnRight(float step) {
    float r = rotate - step;

    if(r < 0.0f) {
        r += R_360;
    }

    setRotate(r);
}
void ViewPoint::moveForward(void) {
    pos_x += dir_x;
    pos_y += dir_y;
}

void ViewPoint::moveBackward(void) {
    pos_x -= dir_x;
    pos_y -= dir_y;
}

void ViewPoint::moveUp(void) {
    pos_z += (int)moveStep;
}

void ViewPoint::moveDown(void) {
    pos_z -= (int)moveStep;
}

void ViewPoint::strafeLeft(void) {
    pos_x -= dir_y;
    pos_y += dir_x;
}

void ViewPoint::strafeRight(void) {
    pos_x += dir_y;
    pos_y -= dir_x;
}
