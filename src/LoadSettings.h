#ifndef LOADSETTINGS_H
#define LOADSETTINGS_H

#include <string>

struct LoadSettings
{
    std::string pwadPath;
    std::string iwadPath;
    std::string map;
};

#endif
