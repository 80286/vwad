#ifndef COMMON_H
#define COMMON_H

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>
#include "ctype.h"
#include <cstdarg>

#include "Constants.h"

#define DEG2RAD ((2 * M_PI) / 360.0)
#define RAD2DEG (360.0 / (2 * M_PI))
#define EPS 1e-6

template <class T>
T max(T a, T b) {
	return a > b ? a : b;
}

template <class T>
T min(T a, T b) {
	return a < b ? a : b;
}

template <class T>
T clamp(T x, T minval, T maxval) {
    if (x < minval) {
        x = minval;
    }

    if (x >= maxval) {
        x = maxval;
    }

    return x;
}

template <class T>
bool inRange(T c, T a, T b) {
    return c >= a ? (c <= b) : 0;
}

int mod(int a, int n);
float modf(float a, float n);

int sg(int x);

void *Malloc(size_t size);
void Free(void *block);

int isFile(const char *name);
char *str8(char *name);
const char *getField(const char *str, const char sym, const int field);

float pointToAngle(const float x, const float y);

float rad(float t);
int rad2(float &t);
int radToDeg(float t);

int angleToX(const int screenWidth, const float angle);
float xToAngle(const int x, const int screenWidth);

#endif
