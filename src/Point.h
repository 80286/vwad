#ifndef POINT_H
#define POINT_H

struct Point {
    void set(const float x, const float y) { m_x = x; m_y = y; }
    void setX(const float x) { m_x = x; }
    void setY(const float y) { m_y = y; }

    float m_x = 0;
    float m_y = 0;
};

#endif
