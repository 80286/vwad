#ifndef VIEW2D_H
#define VIEW2D_H

#include "Canvas.h"
#include "Color.h"
#include "InputHandler.h"
#include "Point.h"
#include "ViewPoint.h"
#include "Wad.h"

#include <cmath>
#include <cstdint>

class View2d : public InputHandler {
public:
    View2d(Canvas* canvas);

    void draw(const ViewPoint* viewPoint);
    void setMapWad(Wad* wad);
    void setFocusPoint(int x, int y);
    void inverseTransformVertex(const int x, const int y, int& ax_t, int& ay_t);

private:
    void reset();

    void transformVertex(const int x, const int y, int &ax_t, int& ay_t);

    void drawLine(int, int, int, int, const Color*);
    void clipAndDrawLine(int, int, int, int, const Color*);
    void drawPoint(int, int, const Color*);
    void drawViewPoint(const ViewPoint*);
    void drawLineNormal(int, int, int, int, const Color*);
    void drawGrid();

    void handleKeyPress(const KeyConstant key) override {}
    void handleMousePress(const Point* point, const MouseConstant mouseButton) override;
    void handleMouseMove(const Point* point) override;
    void handleMouseRelease(const Point* point, const MouseConstant mouseButton) override;
    void handleMouseWheel(const int delta) override;

    Canvas* m_canvas;
    Wad* m_mapWad;

    int m_windowWidth;
    int m_windowHeight;

    int m_posX;
    int m_posY;

    int m_lastPosX;
    int m_lastPosY;

    int m_scaleXPos;
    int m_scaleYPos;

    int m_selectedLine;

    bool m_draggedRight;

    Point m_draggedPos;

    Color c_axis;
    Color c_grid;
    Color c_vertex;
    Color c_line;
    Color c_line_selected;
    Color c_line_twoSided;
    Color c_viewPoint;

    int m_prevWidth;
    int m_prevHeight;

    int m_gridSize;
    float m_scale;
};

#endif
