#ifndef SDLBASEAPP_H
#define SDLBASEAPP_H

#include <SDL2/SDL.h>
#include "Constants.h"

class SdlBaseApp {
public:
    SdlBaseApp(const int width, const int height);

    void InitSdl();
    void CloseSdl();
    void Render(const uint8_t* pixels, const int pitch);

    KeyConstant GetGenericKeyCode(const int sdlKeyConstant);
    MouseConstant GetGenericMouseCode(const int sdlMouseConstant);

    const int m_width;
    const int m_height;

    SDL_Renderer* m_sdlRenderer;
    SDL_Window* m_sdlWindow;
    SDL_Texture* m_sdlTexture;
};

#endif
