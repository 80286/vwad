#include "View2d.h"
#include "Constants.h"

View2d::View2d(Canvas* canvas) : m_canvas(canvas) {
    m_windowWidth = m_canvas->getWidth();
    m_windowHeight = m_canvas->getHeight();

    m_prevWidth = -1;
    m_prevHeight = -1;

    reset();

    c_vertex.setRgb(120, 120, 200);
    c_axis.setRgb(0, 160, 160);
    c_grid.setRgb(60, 60, 60);

    c_line.setRgb(200, 200, 200);
    c_line_twoSided.setRgb(100, 100, 100);
    c_line_selected.setRgb(200, 0, 0);

    c_viewPoint.setRgb(100, 100, 100);

    m_draggedRight = false;
    m_draggedPos.setX(0);
    m_draggedPos.setY(0);

    drawGrid();

    setFocusPoint(0, 0);
}

void View2d::setMapWad(Wad* wad) {
    m_mapWad = wad;
}

void View2d::reset() {
    m_posX = 0;
    m_posY = 0;
    m_lastPosX = 0;
    m_lastPosY = 0;
    m_scale = 1.0f;
    m_scaleXPos = 0;
    m_scaleYPos = 0;
    m_gridSize = 64;
    m_selectedLine = 0;
}

void View2d::setFocusPoint(int x, int y) {
    int middleX, middleY;
    int cornerX, cornerY;

    inverseTransformVertex(m_canvas->getWidth(), 0, cornerX, cornerY);
    inverseTransformVertex(
            m_canvas->getWidth()  / 2,
            m_canvas->getHeight() / 2, 
            middleX, middleY);

    const int newX = x - abs(cornerX - middleX);
    const int newY = y - abs(cornerY - middleY);

    m_posX = newX;
    m_posY = newY;
    m_lastPosX = newX;
    m_lastPosY = newY;
}

void View2d::handleMousePress(const Point* point, const MouseConstant mouseButton) {
	if(mouseButton == MC_RightButton) {
        m_draggedRight = true;
        m_draggedPos = *point;
    } else if(mouseButton == MC_LeftButton) {
        const int sx = point->m_x;
        const int sy = point->m_y;

        int ox, oy;
        inverseTransformVertex(sx, sy, ox, oy);
        printf("view:setPos(); (%d, %d) -> (%d, %d);\n", sx, sy, ox, oy);
    }
}

void View2d::handleMouseMove(const Point* pos) {
    if(m_draggedRight) {
        m_posX = (m_lastPosX +  (m_draggedPos.m_x - pos->m_x)) / m_scale;
        m_posY = (m_lastPosY + -(m_draggedPos.m_y - pos->m_y)) / m_scale;
        return;
    }

    if(!m_canvas->isInCanvas(pos->m_x, pos->m_y)) {
        return;
    }
}

void View2d::handleMouseRelease(const Point* pos, const MouseConstant mouseButton) {
    if(m_draggedRight) {
        m_draggedRight = false;

        m_lastPosX = m_posX * m_scale;
        m_lastPosY = m_posY * m_scale;
    }
}

void View2d::handleMouseWheel(const int delta) {
    static const float scaleValues[] = {0.0625, 0.125f, 0.25f, 0.5f, 1.0f, 1.5f, 2.0f, 2.5f, 3.0f, 3.5f, 4.0f};
    static int scaleIndex = 3;

    const int numDegrees = delta / 8;
    const int numSteps = numDegrees / 15;

    if(numSteps > 0) {
        scaleIndex = min(scaleIndex + 1, (int)(sizeof(scaleValues) / sizeof(scaleValues[0]) - 1));
    } else if(numSteps < 0) {
        scaleIndex = max(scaleIndex - 1, 0);
    }
    m_scale = scaleValues[scaleIndex];

    m_lastPosX = m_posX * m_scale;
    m_lastPosY = m_posY * m_scale;
}

void View2d::drawGrid() {
    const int ymax = m_canvas->getHeight() - 1;
    const int xmax = m_canvas->getWidth() - 1;

    int originX, originY;
    /* Get screen coords of origin: */
    transformVertex(0, 0, originX, originY);

    const int scaledGridSize = (int)((float)m_gridSize * m_scale);

    /* Draw horizontal lines: */
    for(int i = originY % scaledGridSize ; i < ymax; i += scaledGridSize) {
        clipAndDrawLine(0, i, xmax, i, &c_grid);
    }

    /* Draw vertical lines: */
    for(int i = originX % scaledGridSize; i < xmax; i += scaledGridSize) {
        clipAndDrawLine(i, 0, i, ymax, &c_grid);
    }

    /* Draw X axis: */
    if(inRange(originY, 0, ymax)) {
        clipAndDrawLine(0, originY, xmax, originY, &c_axis);
    }

    /* Darw Y axis: */
    if(inRange(originX, 0, ymax)) {
        clipAndDrawLine(originX, 0, originX, ymax, &c_axis);
    }
}

void View2d::transformVertex(const int x, const int y, int &ax_t, int &ay_t) {
    float x_t = (float)x;
    float y_t = (float)y;

    x_t -= (float)m_posX;
    y_t -= (float)m_posY;

    x_t *= m_scale;
    y_t *= m_scale;

    ax_t = (int)x_t;
    ay_t = (int)y_t;

    ay_t = m_canvas->getHeight() - ay_t;
}

void View2d::inverseTransformVertex(const int x, const int y, int &ax_t, int &ay_t) {
    float x_t = (float)x;
    float y_t = (float)(m_canvas->getHeight() - y);

    x_t /= m_scale;
    y_t /= m_scale;

    x_t += (float)m_posX;
    y_t += (float)m_posY;

    ax_t = (int)x_t;
    ay_t = (int)y_t;
}

void View2d::drawLine(int x1, int y1, int x2, int y2, const Color* color) {
    m_canvas->drawLine(x1, y1, x2, y2, color->red(), color->green(), color->blue());
}

void View2d::clipAndDrawLine(int x1, int y1, int x2, int y2, const Color* color) {
    if(!m_canvas->clipSegmentToScreen(x1, y1, x2, y2)) {
        return;
    }

    m_canvas->drawLine(x1, y1, x2, y2, color->red(), color->green(), color->blue());
}

void View2d::drawPoint(int x, int y, const Color* color) {
    m_canvas->drawPoint(x, y, color->red(), color->green(), color->blue());
}

void View2d::drawViewPoint(const ViewPoint* view) {
    const float r45 = 0.25f * M_PI;

    const float dirLength  = m_scale * 45.0f;
    const float armLength = (dirLength / cosf(r45));

    const float dx = view->pos_x;
    const float dy = view->pos_y;
    const float sv = view->rot_sin;
    const float cv = view->rot_cos;
    const float rot = view->rotate;

    int sx, sy;
    /* Coords of player position S(sx, sy): */
    transformVertex(dx, dy, sx, sy);

    if(sy < 0) {
        return;
    }

    /* Coords of direction line P(px, py): */
    const int px = sx + (int)(dirLength * cv);
    const int py = sy - (int)(dirLength * sv);

    /* Coords of left arm L(lx, ly): */
    const int lx = sx + (int)(armLength * cosf(rot - r45));
    const int ly = sy - (int)(armLength * sinf(rot - r45));

    /* Coords of right arm R(rx, ry): */
    const int rx = sx + (int)(armLength * cosf(rot + r45));
    const int ry = sy - (int)(armLength * sinf(rot + r45));

    const int sPos = m_canvas->getPointPosition(sx, sy);
    const int pPos = m_canvas->getPointPosition(px, py);

    if(sPos == POS_INSIDE) {
        drawPoint(sx, sy, &c_viewPoint);
    }

    if(pPos == POS_INSIDE) {
        drawPoint(px, py, &c_viewPoint);
    }

    /* Draw direction vector S -> P: */
    clipAndDrawLine(sx, sy, px, py, &c_viewPoint);

    /* Draw left arm S -> L: */
    clipAndDrawLine(sx, sy, lx, ly, &c_viewPoint);

    /* Draw right arm S -> R: */
    clipAndDrawLine(sx, sy, rx, ry, &c_viewPoint);

    /* Connect left and right arm L -> R: */
    clipAndDrawLine(lx, ly, rx, ry, &c_viewPoint);
}

void View2d::drawLineNormal(int ax, int ay, int bx, int by, const Color* color) {
    const float angle1 = pointToAngle(ax, ay);
    const float angle2 = pointToAngle(bx, by);

    const float lineSpan = angle1 - angle2;
    float normal = 0.0f;

    if(lineSpan < 0.0f ) normal += R_360;
    if(lineSpan > R_360) normal -= R_360;

    const int side = (lineSpan > R_180) ? 1 : 0;

    /*
     * A - B = (dx, dy) = (ax - bx, ay - by);
     *
     * Rotation matrix (PI/2):
     * [ 0 -1 ] [ x ]   [ -y ]
     * [ 1  0 ] [ y ] = [  x ]
     *
     * Rotation matrix (-PI/2):
     * [ 0  1 ] [ x ]   [  y ]
     * [-1  0 ] [ y ] = [ -x ]
     *
     */
    int dx = ax - bx;
    int dy = ay - by;

    if(side == 0) {
        normal = pointToAngle(-dy, dx);
    } else {
        normal = pointToAngle(dy, -dx);
    }

    const int mx = (int)((ax + bx) / 2.0f);
    const int my = (int)((ay + by) / 2.0f);

    dx = mx + (int)(10.0f * cosf(normal));
    dy = my + (int)(10.0f * sinf(normal));

    {
        int mx_s, my_s;
        int dx_s, dy_s;

        transformVertex(mx, my, mx_s, my_s);
        transformVertex(dx, dy, dx_s, dy_s);

        clipAndDrawLine(mx_s, my_s, dx_s, dy_s, color);
    }
}

void View2d::draw(const ViewPoint* viewPoint) {
    m_canvas->clear();

    drawGrid();

    auto& lines = m_mapWad->GetLines();

    if(!m_mapWad->isOpen() || lines.empty()) {
        return;
    }

    for(auto& line: lines) {
        const short ax = line.va->x;
        const short ay = line.va->y;

        const short bx = line.vb->x;
        const short by = line.vb->y;

        int ax_s, ay_s;
        int bx_s, by_s;
        transformVertex(ax, ay, ax_s, ay_s);
        transformVertex(bx, by, bx_s, by_s);

        const int aPos = m_canvas->getPointPosition(ax_s, ay_s);
        const int bPos = m_canvas->getPointPosition(bx_s, by_s);

        if(!m_canvas->clipSegmentToScreen(ax_s, ay_s, bx_s, by_s, aPos, bPos)) {
            continue;
        }

        Color* lineColor;

        if(line.inFront) {
            lineColor = &c_line_selected;
        } else {
            if(line.lside != NULL && line.rside != NULL) {
                lineColor = &c_line_twoSided;
            } else {
                lineColor = &c_line;
            }
        }

        drawLineNormal(ax, ay, bx, by, lineColor);
        drawLine(ax_s, ay_s, bx_s, by_s, lineColor);

        if(aPos == POS_INSIDE) {
            drawPoint(ax_s, ay_s, &c_vertex);
        }

        if(bPos == POS_INSIDE) {
            drawPoint(bx_s, by_s, &c_vertex);
        }
    }

    drawViewPoint(viewPoint);
}
