#ifndef VIEWCONTROLLER_H
#define VIEWCONTROLLER_H

#include "LoadSettings.h"
#include "InputHandler.h"
#include "View3d.h"
#include "View2d.h"

#include <memory>
#include <string>
#include <cstdint>

enum ViewMode {
    MODE_2D = 0,
    MODE_3D = 1,
    MODE_TEX = 2
};

class ViewController : public InputHandler {
public:
    ViewController(const int width, const int height);
    ~ViewController();

    void handleKeyPress(const KeyConstant key) override;
    void handleMousePress(const Point* point, const MouseConstant mouseButton) override;
    void handleMouseMove(const Point* point) override;
    void handleMouseRelease(const Point* point, const MouseConstant mouseButton) override;
    void handleMouseWheel(const int delta) override;

    void draw();
    void toggleMode();
    void loadWadFiles(const LoadSettings& settings);

    uint8_t getBytesPerPixel() const { return m_bytesPerPixel; }
    uint8_t* getBytes() { return m_bytes; }
    void renderImage(Canvas* output, const int ox, const int oy, const Image *img);

private:
    InputHandler* getCurrentInputHandler();

    View3d* m_view3d;
    View2d* m_view2d;

    Wad m_pwad;
    Wad m_iwad;

    Canvas* m_canvas;

    ViewPoint m_viewPoint;
    ViewMode m_mode;

    const uint8_t m_bytesPerPixel = 4;
    uint8_t* m_bytes;
    uint32_t m_width;
    uint32_t m_height;
};

#endif
