#include "ViewController.h"
#include "Wad.h"

ViewController::ViewController(const int width, const int height)
    : m_width(width)
    , m_height(height)
    , m_mode(MODE_2D) {

    m_bytes = new uint8_t[width * height * m_bytesPerPixel];
    m_canvas = new Canvas(m_bytes, width, height, m_bytesPerPixel);

    m_view2d = new View2d(m_canvas);
    m_view3d = new View3d(m_canvas);
}

ViewController::~ViewController() {
    delete m_bytes;
    delete m_canvas;
    delete m_view2d;
    delete m_view3d;
}

void ViewController::loadWadFiles(const LoadSettings& settings) {
    const std::string iwadPath = settings.iwadPath.empty() ? "../res/doom1.wad" : settings.iwadPath;
    const std::string pwadPath = settings.pwadPath.empty() ? "../res/test2.wad" : settings.pwadPath;
    const std::string map = settings.map.empty() ? "E1M1" : settings.map;

    Wad* mapWad = &m_iwad;

    if(!iwadPath.empty()) {
        assert(m_iwad.open(iwadPath.c_str()));
        m_iwad.read();
        m_iwad.prepareTextures(&m_iwad);
    }

    if(!pwadPath.empty() && pwadPath != iwadPath) {
        mapWad = &m_pwad;
        assert(mapWad->open(pwadPath.c_str()));
        mapWad->read();
    }

    m_view3d->setIWAD(&m_iwad);
    m_view3d->setPWAD(mapWad);
    m_view2d->setMapWad(mapWad);

    mapWad->prepareLines(map.c_str());
    mapWad->findTextures(&m_iwad);

    const auto playerObj = mapWad->getPlayer(map.c_str());
    m_viewPoint.setPos(playerObj->x, playerObj->y);
    m_viewPoint.setRotate((float)playerObj->angle * DEG2RAD);
    m_view2d->setFocusPoint(playerObj->x, playerObj->y);
}

void ViewController::handleKeyPress(const KeyConstant key) {
    switch(key) {
    case KC_W:
    case KC_Up:
        m_viewPoint.moveForward();
        break;
    case KC_S:
    case KC_Down:
        m_viewPoint.moveBackward();
        break;
    case KC_A:
        m_viewPoint.strafeLeft();
        break;
    case KC_D:
        m_viewPoint.strafeRight();
        break;
    case KC_Left:
        m_viewPoint.turnLeft();
        break;
    case KC_Right:
        m_viewPoint.turnRight();
        break;
    case KC_PageUp:
        m_viewPoint.moveUp();
        break;
    case KC_PageDown:
        m_viewPoint.moveDown();
        break;
    case KC_R:
        toggleMode();
        return;
    case KC_L:
        m_view3d->toggleLighting();
        break;
    case KC_F1:
        m_view3d->toggleDetailsLevel();
        break;
    default:
        break;
    }

    m_view2d->setFocusPoint(m_viewPoint.pos_x, m_viewPoint.pos_y);
}

InputHandler* ViewController::getCurrentInputHandler() {
    return (m_mode == MODE_2D) ? (InputHandler*)m_view2d : (InputHandler*)m_view3d;
}

void ViewController::handleMousePress(const Point* point, const MouseConstant mouseButton) {
    getCurrentInputHandler()->handleMousePress(point, mouseButton);

    if(mouseButton == MC_LeftButton) {
        const int sx = point->m_x;
        const int sy = point->m_y;

        int ox, oy;
        m_view2d->inverseTransformVertex(sx, sy, ox, oy);
        m_viewPoint.pos_x = ox;
        m_viewPoint.pos_y = oy;
    }
}

void ViewController::handleMouseMove(const Point *pos) {
    getCurrentInputHandler()->handleMouseMove(pos);
}

void ViewController::handleMouseRelease(const Point *pos, const MouseConstant mouseButton) {
    getCurrentInputHandler()->handleMouseRelease(pos, mouseButton);
}

void ViewController::handleMouseWheel(const int delta) {
    getCurrentInputHandler()->handleMouseWheel(delta);
}

void ViewController::toggleMode() {
    m_mode = (m_mode == MODE_2D) ? MODE_3D : MODE_2D;
}

void ViewController::draw() {
    if(m_mode == MODE_2D) {
        m_view2d->draw(&m_viewPoint);
    } else {
        m_view3d->draw(&m_viewPoint);
    }
}

void ViewController::renderImage(Canvas* output, const int ox, const int oy, const Image *img) {
    for(int x = 0; x < img->m_width; ++x) {
        for(int y = 0; y < img->m_height; ++y) {
            const int pos = (img->m_width * y) + x;

            if(img->m_data[pos] == 247) {
               output->setPixel(x, y, 0, 255, 255);
               continue;
            }

            const int fx = ox + x;
            const int fy = oy + y;

            if(output->isInCanvas(fx, fy)) {
                uint8_t* pixelData = output->getPixelData(fx, fy);

                m_iwad.paletteIndexToRGB(img->m_data[pos], pixelData);
            }
        }
    }
}
