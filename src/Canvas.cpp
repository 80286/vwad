#include "Canvas.h"
#include "common.h"
#include <cstring>
#include <cmath>

Canvas::Canvas(uint8_t *bits, const int w, const int h, const int bytesPerPixel)
    : m_bytesPerPixel(bytesPerPixel)
    , m_width(w)
    , m_height(h) {

    m_data = bits;
    m_data_end = bits + m_bytesPerPixel * (w * h);
}

void Canvas::clear() {
    memset(m_data, (uint8_t)0, (m_width * m_height) * m_bytesPerPixel);
}

int Canvas::getWidth() const {
    return m_width;
}

int Canvas::getHeight() const {
    return m_height;
}

int Canvas::getPixelIndex(int x, int y) {
	return m_bytesPerPixel * (m_width * y + x);
}

bool Canvas::isInCanvas(int x, int y) {
    return (x >= 0 && x < m_width && y >= 0 && y < m_height);
}

uint8_t* Canvas::getPixelData(const int x, const int y) {
    return m_data + getPixelIndex(x, y);
}

void Canvas::setPixel(int x, int y, int r, int g, int b) {
    if(!isInCanvas(x, y)) {
        return;
    }

    const int p = getPixelIndex(x, y);

	m_data[p + 0] = b;
	m_data[p + 1] = g;
	m_data[p + 2] = r;
}

int Canvas::getPointPosition(int x, int y) {
    const int xmin = 0;
    const int ymin = 0;
    const int xmax = m_width  - 1;
    const int ymax = m_height - 1;

    int result = POS_INSIDE;

    if(x < xmin) {
        result |= POS_LEFT;
    } else if(x > xmax) {
        result |= POS_RIGHT;
    } 

    if(y < ymin) {
        result |= POS_BOTTOM;
    } else if(y > ymax) {
        result |= POS_TOP;
    }

    return result;
}

bool Canvas::clipSegmentToScreen(int &ax, int &ay, int &bx, int &by) {
    const int aPos = getPointPosition(ax, ay);
    const int bPos = getPointPosition(bx, by);

    return clipSegmentToScreen(ax, ay, bx, by, aPos, bPos);
}

bool Canvas::clipSegmentToScreen(int &ax, int &ay, int &bx, int &by, int aPos, int bPos) {
    /* https://en.wikipedia.org/wiki/Cohen%E2%80%93Sutherland_algorithm */
    const int xmin = 0;
    const int ymin = 0;
    const int xmax = m_width  - 1;
    const int ymax = m_height - 1;

    while(true) {
        if(aPos == POS_INSIDE && bPos == POS_INSIDE) {
            /* Points are inside screen: */
            return true;
        } else if(aPos & bPos) {
            /* Points are in the same zones: */
            return false;
        } else {
            /* At least one point is behind screen: */
            int pos = aPos ? aPos : bPos;

            int x = 0;
            int y = 0;

            /* Clip segment to each edge in single iteration: */
            if(pos & POS_TOP) {
                x = ax + ((ymax - ay) * (bx - ax)) / (by - ay);
                y = ymax;
            } else if(pos & POS_BOTTOM) {
                x = ax + ((ymin - ay) * (bx - ax)) / (by - ay);
                y = ymin;
            } else if(pos & POS_RIGHT) {
                x = xmax;
                y = ay + ((xmax - ax) * (by - ay)) / (bx - ax);
            } else if(pos & POS_LEFT) {
                x = xmin;
                y = ay + ((xmin - ax) * (by - ay)) / (bx - ax);
            }

            /* Save clipped coordinates: */
            if(pos == aPos) {
                ax = x;
                ay = y;
                aPos = getPointPosition(ax, ay);
            } else {
                bx = x;
                by = y;
                bPos = getPointPosition(bx, by);
            }
        }
    }

    return true;
}

void Canvas::drawLine(int x1, int y1, int x2, int y2, int cr, int cg, int cb) {
	const float a = (float)(y2 - y1) / (float)(x2 - x1);
	const float b = (float)y2 - (a * (float)x2);

	if(fabs(a) >= 1.0) {
		const float m1 = min(y1, y2);
		const float m2 = max(y1, y2);

		for(float y = m1; y <= m2; y += 1.0f) {
			float x = (x2 - x1) != 0 ? ((y - b) / a) : x1;
			setPixel(x, y, cr, cg, cb);
		}
	} else {
		const float m1 = min(x1, x2);
		const float m2 = max(x1, x2);

		for(float x = m1; x <= m2; x += 1.0f) {
			setPixel(x, a * x + b, cr, cg, cb);
		}
	}
}

void Canvas::drawPoint(int x, int y, int r, int g, int b) {
    for(int j =  2; j > -2; --j) {
        for(int i = -2; i < 3; ++i) {
            const int nx = x + i;
            const int ny = y + j;

            if(isInCanvas(nx, ny)) {
                setPixel(nx, ny, r, g, b);
            }
        }

        for(int i = -2; i < 3; ++i) {
            const int nx = x + i;
            const int ny = y + j;

            if(isInCanvas(nx, ny)) {
                setPixel(nx, ny, r, g, b);
            }
        }
    }
}
