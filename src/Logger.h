#ifndef LOGGER_H
#define LOGGER_H

#include <cstdarg>

enum MsgLevel {
    ML_DEBUG = 0,
    ML_INFO = 1,
    ML_WARNING = 2,
    ML_ERROR = 3,
    ML_SILENT = 4
};

class Logger {
public:
    void msg(int, const char *, va_list) const;
    void msgDebug(const char *, ...) const;
    void msgWarning(const char *, ...) const;
    void msgError(const char *, ...) const;
    void msgInfo(const char *, ...) const;

    MsgLevel m_msgLevel;
};

#endif
