#ifndef WAD_H
#define WAD_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include "common.h"
#include <map>
#include <string>
#include <vector>

#include "Logger.h"

enum MapLump {
    ML_THINGS,
    ML_LINEDEFS,
    ML_SIDEDEFS,
    ML_VERTEXES,
    ML_SEGS,
    ML_SECTORS,
    ML_SSECTORS,
    ML_NODES,
};

enum GfxLump {
    GL_COLORMAP,
    GL_PLAYPAL,
    GL_PNAMES,
    GL_TEXTURE1,
    GL_TEXTURE2
};

/* http://doom.wikia.com/wiki/WAD 
 * https://github.com/id-Software/DOOM/blob/master/linuxdoom-1.10/w_wad.h */
struct wadinfo_t {
    char id[4];
    int numlumps;
    int infotableofs;
};

struct filelump_t {
    char name[8];
    int filepos;
    int size;
    void *data;
    int data_len;
};

struct thing_t {
    /* Doom format */
    short x;
    short y;
    short angle;
    short type;
    short flags;
};

struct linedef_t {
    /* Doom format */
    short start;
    short end;
    short flags;
    short type;
    short sectortag;
    short rightsdef;
    short leftsdef;
};

struct sidedef_t {
    short x;
    short y;
    char uppertex[8];
    char lowertex[8];
    char middletex[8];
    short sector;
};

struct vertex_t {
    short x;
    short y;
};

struct seg_t {
    short start;
    short end;
    short angle;
    short linedef;
    short direction;
    short offset;
};

struct sector_t {
    short floorheight;
    short ceilheight;
    char floortex[8];
    char ceiltex[8];
    short lightlevel;
    short type;
    short tag;
};

struct subsector_t {
    short segcount;
    short seg;
};

struct node_bbox_t {
    short top;
    short bottom;
    short left;
    short right;
};

struct node_t {
    short x_part;
    short y_part;
    short cx;
    short cy;
    node_bbox_t rbox;
    node_bbox_t lbox;
    short left;
    short right;
};

/* http://doom.wikia.com/wiki/TEXTURE1_and_TEXTURE2 */
struct mappatch_t {
    short originx;
    short originy;
    short patch;
    short stepdir;
    short colormap;
}; // 10

struct maptexture_t {
    char name[8];
    int masked; /* Unused. */
    short width;
    short height;
    int columndirectory; /* Unused. */
    short patchcount;

    mappatch_t *patches;
}; // 22

struct texture_t {
    int numtextures;

    maptexture_t *mtexture;
};

class Image {
public:
    Image();
    Image(const int width, const int height, uint8_t* data, const char* name);

    static void freeImage(Image*);

    short m_width;
    short m_height;

    unsigned char* m_data;
    char m_name[9];
};

struct LumpDef {
    const char *name;
    void (*func)(void*, FILE*, filelump_t*);
};

sector_t *getSectorByTag(sector_t *array, int length, short tag);
thing_t *getThingByType(thing_t *things, int things_len, short type);

struct sidedef_info_t {
    Image *uTex;
    Image *mTex;
    Image *lTex;

    Image *fTex;
    Image *cTex;
};

struct line_t {
    vertex_t *va;
    vertex_t *vb;
    sidedef_t *lside;
    sidedef_t *rside;
    sector_t *lsector;
    sector_t *rsector;
    linedef_t *linedef;

    sidedef_info_t front;
    sidedef_info_t back;

    bool inFront;
    bool isOneSided();
};

class Wad {
public:
     Wad();
    ~Wad();

    bool isOpen() const;

    bool open(const char *filename);
    int read();

    void prepareLines(const char* mapName);
    void prepareTextures(Wad* texWad);
    void findTextures(Wad* texwad);

    Image* getTexture(const char* name);
    Image* getFlat(const char *name) const;
    const thing_t* getPlayer(const char* map);

    void paletteIndexToRGB(const unsigned char index, unsigned char* color);
    unsigned char getColormapIndex(const unsigned int map, const unsigned char paletteIndex);

    static bool isMapName(const char *str);

    std::vector<line_t>& GetLines() { return m_lines; }

private:
    void close();

    std::vector<filelump_t>::iterator getLumpIterator(const char* name);
    filelump_t* getLump(const char* name);
    filelump_t* getMapLump(const char* mapName, const char* name);

    Image* getTextureFromArray(std::vector<Image>& textures, const char* name);

    void buildTexturesArray(std::vector<Image>& textures, texture_t* texture_lump);

    void initData();

    void prepareTexture(std::vector<Image>& textures, Wad* wad, const GfxLump textureLumpId, const char* textureLumpName);

    void readPatches();
    void readFlats();
    void listContent();

    void freeData();
    void freeTexturesLump(texture_t *tex);
    int freeImagesArray(std::vector<Image>& images);

    static void readThings(void* self, FILE *f, filelump_t *);
    static void readLinedefs(void* self, FILE *f, filelump_t *);
    static void readSidedefs(void* self, FILE *f, filelump_t *);
    static void readVertexes(void* self, FILE *f, filelump_t *);
    static void readSegments(void* self, FILE *f, filelump_t *);
    static void readSectors(void* self, FILE *f, filelump_t *);
    static void readSubSectors(void* self, FILE *f, filelump_t *);
    static void readNodes(void* self, FILE *f, filelump_t *);

    static void readPlaypal(void* self, FILE *f, filelump_t *);
    static void readColormap(void* self, FILE *f, filelump_t *);
    static void readPNames(void* self, FILE *f, filelump_t *);
    static void readTextures(void* self, FILE *f, filelump_t *);

    static void thingInfo(void* self, thing_t *thing);
    static void linedefInfo(void* self, linedef_t *linedef);
    static void sidedefInfo(void* self, sidedef_t *sidedef);
    static void vertexInfo(void* self, vertex_t *vertex);
    static void segInfo(void* self, seg_t *seg);
    static void sectorInfo(void* self, sector_t *sector);
    static void subSectorInfo(void* self, subsector_t *subsector);
    static void nodeInfo(void* self, node_t *node);

    unsigned char m_palette[768];

    /* Colormap (256 * 34): */
    unsigned char* m_colormap = NULL;

    std::map<MapLump, LumpDef> m_mapReadFunctions;
    std::map<GfxLump, LumpDef> m_gfxReadFunctions;

    Logger m_logger;
    FILE *m_f;

    std::vector<filelump_t> m_dir;
    std::vector<line_t> m_lines;
    std::vector<Image> m_textures1;
    std::vector<Image> m_textures2;
    std::vector<Image*> m_flats;
    std::vector<Image*> m_patches;

    std::string m_filename;
    std::string m_filenameBase;

    wadinfo_t m_info;

    const char* chosenMap;
};

#endif
