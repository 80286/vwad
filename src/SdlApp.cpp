#include "SdlApp.h"

SdlApp::SdlApp(const int width, const int height)
    : SdlBaseApp(width, height) {
    m_viewController = new ViewController(width, height);
}

SdlApp::~SdlApp() {
    delete m_viewController;
}

void SdlApp::Init(const LoadSettings& settings) {
    m_viewController->loadWadFiles(settings);
}

void SdlApp::BeginLoop() {
    bool done = false;

    InitSdl();

    while(!done) {
        SDL_Event event;

        while(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT) {
                done = true;
            }

            if(event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(m_sdlWindow)) {
                done = true;
            }

            Point p;
            MouseConstant mouseButton;

            switch(event.type) {
            case SDL_KEYDOWN:
                m_viewController->handleKeyPress(GetGenericKeyCode(event.key.keysym.sym));
                break;

            case SDL_MOUSEBUTTONDOWN:
                p.set(event.motion.x, event.motion.y);
                m_viewController->handleMousePress(&p, GetGenericMouseCode(event.button.button));
                break;

            case SDL_MOUSEBUTTONUP:
                p.set(event.motion.x, event.motion.y);
                m_viewController->handleMouseRelease(&p, GetGenericMouseCode(event.button.button));
                break;

            case SDL_MOUSEMOTION:
                p.set(event.motion.x, event.motion.y);
                m_viewController->handleMouseMove(&p);
                break;

            case SDL_MOUSEWHEEL:
                m_viewController->handleMouseWheel(event.wheel.y);
                break;
            }
        }

        m_viewController->draw();

        Render(m_viewController->getBytes(), m_width * m_viewController->getBytesPerPixel());
    }

    CloseSdl();
}
