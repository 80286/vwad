#ifndef COLOR_H
#define COLOR_H

#include <cstdint>

struct Color {
    void setRgb(uint8_t r, uint8_t g, uint8_t b) {
        m_r = r;
        m_g = g;
        m_b = b;
    }

    uint8_t red() const { return m_r; }
    uint8_t green() const { return m_g; }
    uint8_t blue() const { return m_b; }

    uint8_t m_r = 0, m_g = 0, m_b = 0;
};

#endif
