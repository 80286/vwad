#include "common.h"

static unsigned long allocated = 0;

void *Malloc(size_t msize) {
    void *block;

    if((block = malloc(msize)) == NULL) {
        fprintf(stderr, "Malloc(); Unable to allocate %zu bytes!\n", msize);
        perror("Malloc");
        exit(1);
    }
    allocated++;
    return block;
}

void Free(void *block) {
    free(block);
    allocated--;
    if(allocated == 0) {
        printf("Free(); Allocated = 0;\n");
    }
}

int isFile(const char *name) {
    FILE *f;

    if((f = fopen(name, "r")) == NULL) {
        return 1;
    }
    fclose(f);
    return 0;
}

char *toUpper(char *str) {
    static char buf[9], *bp;

    memset(buf, '\0', 9);
    bp = buf;
    while(*str && (bp - buf) < 8) {
        bp[0] = toupper(str[0]);
        str++;
    }
    return buf;
}

void toUpper2(char *str) {
    while(*str) {
        str[0] = toupper(str[0]);
        str++;
    }
}

char *str8(char *name) {
    static char buf[9];

    memset(buf, '\0', 9);

    /* buf[8] = 0; */
    strncpy(buf, name, 8);
    return buf;
}

int mod(int a, int n) {
    if(a >= 0) {
        return a % n;
    }
    while(a < 0) a += n;

    return a % n;
}

float modf(float a, float n) {
    while(a < 0.0f) a += n;
    while(a >=   n) a -= n;
    return a;
}

int sg(int x) {
    if(x < 0) {
        return -1;
    } else if (x > 0) {
        return 1;
    } else {
        return 0;
    }
}

const char *getField(const char *str, const char sym, const int field) {
    const char* ptr = str + strlen(str) - 1;
    int i = 0;

    while(*ptr && ptr >= str) {
        if(*ptr == sym) {
            if(field == i) {
                return ptr + 1;
            }
            i++;
        }
        ptr--;
    }

    return str;
}

float pointToAngle(const float x, const float y) {
    if(x == 0.0f) {
        return (y > 0.0f) ? R_90 : R_270;
    }

    const float result = atan2f(y, x);
    /* atan2(x, y) \in [-pi, pi]
     *
     * result < 0:
     *         result \in [-pi, 0]
     *      => (result + 2pi) \in [pi, 2pi]
     * result >= 0:
     *      result
     */

    return result < 0.0f ? (result + R_360) : result;
}

float rad(float t) {
    if(t < 0.0f) {
        t += R_360;
    } else if(t >= R_360) {
        t -= R_360;
    }

    return t;
}

int rad2(float &t) {
    if(t < 0.0f) {
        t += R_360;
        return -1;
    } else if(t >= R_360) {
        t -= R_360;
        return 1;
    } else {
        return 0;
    }
}

int radToDeg(float t) {
    return (int)((rad(t) / R_360) * 360.0f);
}

int angleToX(const int screenWidth, const float angle) {
    /* 
     * t \in [-1, 1]
     *
     * tanf(R_90 - R_135 = -R_45) = -1
     * tanf(R_90 - R_45  =  R_45) =  1
     */
    float t = tanf(R_90 - angle);

    /* [-1, 1] -> [0, 1] */
    t = 0.5f + t * 0.5f;

    /*
     * t \in [0, 1]
     * sx = t * screenWidth
     * sx \in  [0, screenWidth]
     */
    return (int)(t * (float)screenWidth);
}

float xToAngle(const int x, const int screenWidth) {
    return (R_90 - atan((2.0f * (float)x) / (float)screenWidth - 1.0f));
}
