#ifndef VIEWPOINT_H
#define VIEWPOINT_H

#include "Constants.h"

static float rotateStep = R_180 / 18.0f;

struct ViewPoint {
    int pos_x;
    int pos_y;
    int pos_z;
    int dir_x;
    int dir_y;
    float rot_sin;
    float rot_cos;
    float rotate;

    ViewPoint(void);

    void setPos(int, int, int);
    void setPos(int, int);
    void setRotate(float);
    void turnLeft(float step=rotateStep);
    void turnRight(float step=rotateStep);
    void moveForward(void);
    void moveBackward(void);
    void moveUp(void);
    void moveDown(void);
    void strafeLeft(void);
    void strafeRight(void);
};

#endif
