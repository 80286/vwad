#ifndef MAPRENDER_H
#define MAPRENDER_H

#include "Canvas.h"
#include "InputHandler.h"
#include "Wad.h"
#include "ViewPoint.h"
#include <cmath>
#include <vector>

struct S_Surface {
    Image *tex;

    int height1;
    int height2;
    int texHeight;
    int flags;

    int x_offset;
    int y_offset;

    unsigned char light;

    S_Surface();
};

struct S_Wall {
    short s_x1;
    short s_x2;

    double iz1;
    double iz2;
    double middleIz;
    double deltaIz;

    float angle1;
    float angle2;
    float deltaAngle;

    float deltaLength;
    float deltaXoffset;

    float wlen;

    float rangle1, rangle2;

    line_t *line;

    int i;
    int side;

    double t_iz;
    float t_angle;
    float t_rangle;
    float t_xoffset;

    S_Surface ceil;
    S_Surface upper; // 2sided
    S_Surface middle;// 1sided
    S_Surface lower; // 2sided
    S_Surface floor;

    void selectSurfaces(int);
    void init();
    void step();
    void calcSpan(int x);
};

class View3d : public InputHandler {
public:
     View3d(Canvas*);
    ~View3d();

    void setPWAD(Wad*);
    void setIWAD(Wad*);

    void draw(const ViewPoint* viewPoint);

    void toggleDetailsLevel();
    void toggleLighting();

    void handleKeyPress(const KeyConstant key) {}
    void handleMousePress(const Point* point, const MouseConstant mouseButton) {}
    void handleMouseMove(const Point* point) {}
    void handleMouseRelease(const Point* point, const MouseConstant mouseButton) {}
    void handleMouseWheel(int delta) {}

private:
    void clearVisibleWalls();

    float YToDistance(const int y, const int height) const;
    int distanceToScreenY(const double, const int) const;
    int angleToScreenX(const float x) const;

    void viewPoint_setDefaults();
    void viewPoint_setRotate(float);

    void drawFlatColumn(S_Surface*, int, int, int);
    void drawWallColumn(S_Wall*, S_Surface*, int, int, int, int, int);
    void drawWallSurface(S_Wall*, S_Surface*, int);
    void drawColumns();


    void clipOneSidedWalls();
    void updateSpanWalls(int screenX);
    void selectVisibleWalls(std::vector<line_t>& lines);

    const ViewPoint* m_viewPoint;
    Canvas* m_canvas;
    Wad* m_pwad;
    Wad* m_iwad;

    std::vector<S_Wall*> m_visibleWalls;

    double* m_zBuffer;
    int m_zBufferLength;

    bool m_draggedRight;

    bool m_cfgLights;
    int  m_cfgDetailLevel;

    int m_prevMouseX;

    int m_oy1;
    int m_oy2;
};

#endif
