#include "Logger.h"
#include <stdio.h>

void Logger::msg(int level, const char *format, va_list vlist) const {
	/* va_list vl; */
    const char *prefix;

    if(level < m_msgLevel) {
        return;
    }
    
    switch(level) {
    case ML_DEBUG:
        prefix = "Debug";
        break;
    case ML_INFO:
        prefix = "Info";
        break;
    case ML_WARNING:
        prefix = "Warning";
        break;
    case ML_ERROR:
        prefix = "Error";
        break;
    default:
        prefix = NULL;
        break;
    }

    /* printf("[%8s] %8s; ", prefix, m_filenameBase == NULL ? "-" : m_filenameBase); */

	/* va_start(vl, f); */
	vprintf(format, vlist);
	/* va_end(vl); */ 
}

void Logger::msgDebug(const char *format, ...) const {
    va_list vl;

	va_start(vl, format);
    msg(ML_DEBUG, format, vl);
	va_end(vl); 
}

void Logger::msgInfo(const char *format, ...) const {
    va_list vl;

	va_start(vl, format);
    msg(ML_INFO, format, vl);
	va_end(vl); 
}

void Logger::msgWarning(const char *format, ...) const {
    va_list vl;

	va_start(vl, format);
    msg(ML_WARNING, format, vl);
	va_end(vl); 
}

void Logger::msgError(const char *format, ...) const {
    va_list vl;

	va_start(vl, format);
    msg(ML_ERROR, format, vl);
	va_end(vl); 
}
