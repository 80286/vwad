#include "SdlBaseApp.h"

SdlBaseApp::SdlBaseApp(const int width, const int height)
    : m_width(width)
    , m_height(height)
    , m_sdlRenderer(nullptr)
    , m_sdlTexture(nullptr)
    , m_sdlWindow(nullptr) {
}

void SdlBaseApp::InitSdl() {
    // Setup SDL
    // (Some versions of SDL before <2.0.10 appears to have performance/stalling issues on a minority of Windows systems,
    // depending on whether SDL_INIT_GAMECONTROLLER is enabled or disabled.. updating to latest version of SDL is recommended!)
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0)
    {
        printf("Error: %s\n", SDL_GetError());
        return;
    }

    // Setup window
    SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    m_sdlWindow = SDL_CreateWindow("", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_width, m_height, window_flags);

    // Setup SDL_Renderer instance
    m_sdlRenderer = SDL_CreateRenderer(m_sdlWindow, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);
    if(m_sdlRenderer == NULL) {
        SDL_Log("Error creating SDL_Renderer!");
        return;
    }

    m_sdlTexture = SDL_CreateTexture(m_sdlRenderer,
        SDL_PIXELFORMAT_ARGB8888,
        SDL_TEXTUREACCESS_STREAMING,
        m_width, m_height);

    SDL_RendererInfo info;
    SDL_GetRendererInfo(m_sdlRenderer, &info);
    SDL_Log("Current SDL_Renderer: %s; flags: %d", info.name, info.flags);
}

void SdlBaseApp::CloseSdl() {
    SDL_DestroyTexture(m_sdlTexture);
    SDL_DestroyRenderer(m_sdlRenderer);
    SDL_DestroyWindow(m_sdlWindow);

    SDL_Quit();
}

void SdlBaseApp::Render(const uint8_t* pixels, const int pitch) {
    SDL_SetRenderDrawColor(m_sdlRenderer, 0, 0, 0, 255);
    SDL_RenderClear(m_sdlRenderer);

    SDL_UpdateTexture(m_sdlTexture, NULL, pixels, pitch);
    SDL_RenderCopy(m_sdlRenderer, m_sdlTexture, NULL, NULL);
    SDL_RenderPresent(m_sdlRenderer);
}

KeyConstant SdlBaseApp::GetGenericKeyCode(const int sdlKeyConstant) {
    KeyConstant genericKeyCode = KC_NONE;

    switch(sdlKeyConstant) {
        case SDLK_w: return KC_W;
        case SDLK_s: return KC_S;
        case SDLK_a: return KC_A;
        case SDLK_d: return KC_D;
        case SDLK_r: return KC_R;
        case SDLK_l: return KC_L;
        case SDLK_LEFT: return KC_Left;
        case SDLK_RIGHT: return KC_Right;
        case SDLK_UP: return KC_Up;
        case SDLK_DOWN: return KC_Down;
        case SDLK_PAGEUP: return KC_PageUp;
        case SDLK_PAGEDOWN: return KC_PageDown;
        case SDLK_F1: return KC_F1;
    }

    return genericKeyCode;
}

MouseConstant SdlBaseApp::GetGenericMouseCode(const int sdlMouseConstant) {
    MouseConstant genericMouseConstant = MC_NONE;

    switch(sdlMouseConstant) {
        case SDL_BUTTON_LEFT: return MC_LeftButton;
        case SDL_BUTTON_RIGHT: return MC_RightButton;
    };

    return genericMouseConstant;
}
