#ifndef CANVAS_H
#define CANVAS_H

#include <cstdint>

class Canvas {
public:
    Canvas(uint8_t *, const int, const int, const int);

    uint8_t *m_data;
    uint8_t *m_data_end;

    int getWidth() const;
    int getHeight() const;
    int getPixelIndex(int, int);
    void clear();
    void setPixel(int, int, int, int, int);
    void drawLine(int, int, int, int, int, int, int);
    void drawPoint(int, int, int, int, int);

    int getBytesPerPixel() const { return m_bytesPerPixel; }
    uint8_t* getPixelData(const int x, const int y);
    bool isInCanvas(int, int);
    int getPointPosition(int, int);
    bool clipSegmentToScreen(int &, int &, int &, int &);
    bool clipSegmentToScreen(int &, int &, int &, int &, int, int);

private:
    const int m_width;
    const int m_height;
    const int m_bytesPerPixel = 4;
};

#endif
