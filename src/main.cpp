#include "SdlApp.h"
#include "Wad.h"

#include <string>

static void printUsage(const char *prog) {
    printf(
    "Usage: %s "
    "[-i <Path_to_IWAD>] "
    "[-m <MAP>] "
    "[Path_to_PWAD]\n", 
    prog);
}

struct ArgvSettings {
    LoadSettings loadSettings;
    bool valid;
};

static ArgvSettings parseArgv(const int argc, const char *argv[]) {
    ArgvSettings argvSettings;

    argvSettings.valid = true;

    for(int i = 1; i < argc; ++i) {
        const char start = *argv[i];

        if(!start) {
            continue;
        }

        const char opt = argv[i][1];
        if(!opt) {
            continue;
        }

        if(start != '-') {
            printf("%s: selected PWAD '%s'.\n", __FUNCTION__, argv[i]);
            argvSettings.loadSettings.pwadPath = argv[i];
            continue;
        }

        switch(opt) {
        case 'i':
            if((i + 1) >= argc) {
                printf("%s: missing path to IWAD file for -i parameter.\n", __FUNCTION__);
                continue;
            }

            printf("%s: selected IWAD '%s'.\n", __FUNCTION__, argv[i + 1]);
            argvSettings.loadSettings.iwadPath = argv[i + 1];

            ++i;
            break;
        
        case 'm':
            if((i + 1) >= argc) {
                printf("%s: missing path to PWAD file for -m parameter.\n", __FUNCTION__);
                continue;
            }

            if(!Wad::isMapName(argv[i + 1])) {
                printf("%s: wrong format of map name '%s'.\n", __FUNCTION__, argv[i + 1]);
            } else {
                argvSettings.loadSettings.map = argv[i + 1];
                printf("%s: selected MAP: %s\n", __FUNCTION__, argvSettings.loadSettings.map.c_str());
            }

            ++i;
            break;

        case 'h':
            argvSettings.valid = false;
            break;

        default:
            break;
        }
    }

    return argvSettings;
}

int main(const int argc, const char *argv[]) {
    ArgvSettings argvSettings = parseArgv(argc, argv);

    if(!argvSettings.valid) {
        printUsage(argv[0]);
        return 1;
    }

    SdlApp app(1280, 720);

    app.Init(argvSettings.loadSettings);
    app.BeginLoop();

    return 0;
}
