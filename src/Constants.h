#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <cmath>

const float R_360 = 2.00f * M_PI;
const float R_315 = 1.75f * M_PI;
const float R_270 = 1.50f * M_PI;
const float R_225 = 1.25f * M_PI;
const float R_180 = 1.00f * M_PI;
const float R_135 = 0.75f * M_PI;
const float R_90  = 0.50f * M_PI;
const float R_45  = 0.25f * M_PI;

static const int POS_INSIDE = 0x0;
static const int POS_LEFT   = 0x1;
static const int POS_RIGHT  = 0x2;
static const int POS_TOP    = 0x4;
static const int POS_BOTTOM = 0x8;

enum KeyConstant {
    KC_NONE,
    KC_W,
    KC_S,
    KC_A,
    KC_D,
    KC_R,
    KC_L,
    KC_Left,
    KC_Right,
    KC_Up,
    KC_Down,
    KC_PageUp,
    KC_PageDown,
    KC_F1
};

enum MouseConstant {
    MC_NONE,
    MC_RightButton,
    MC_LeftButton
};

#endif
