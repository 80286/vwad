2D/3D preview tool for Doom maps
=========================

![Screenshot](https://bitbucket.org/80286/vwad/raw/master/res/preview.gif)

How to build & launch
--------------

- `git clone https://bitbucket.org/80286/vwad.git && cd vwad`
- `mkdir build`
- `cd build && cmake ..`
- `make`
- `./vwad`


Keybindings
---------------
- `R` - toggle between 2d and 3d preview
- `W` `S` `A` `D` / cursors - navigation
- `L` - disable/enable ligthing
- `F1` - reduce quality of 3d preview

Implementation of 3d software renderer was based on the source code of [Eureka doom editor](http://eureka-editor.sourceforge.net/).
